package phase6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Shell {
	public static void exec(final String command) {
		ExecutorService pool = Executors.newCachedThreadPool();
		pool.execute(new Runnable() {
			@Override
			public void run() {
				try {
					Process proc = Runtime.getRuntime().exec(command);
					BufferedReader stdInput = new BufferedReader(
							new InputStreamReader(proc.getInputStream()));

					BufferedReader stdError = new BufferedReader(
							new InputStreamReader(proc.getErrorStream()));
					String s;
					// read the output from the command
					System.out.println("Output from command " + command);
					while ((s = stdInput.readLine()) != null) {
						System.out.println(s);
					}

					// read any errors from the attempted command
					System.out.println("Error from Command " + command);
					while ((s = stdError.readLine()) != null) {
						System.out.println(s);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

	}

	private Shell() {
	}
}
