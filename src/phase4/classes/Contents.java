package phase4.classes;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Contents implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6073459353493436080L;
	Map<String, Serializable> contents = new HashMap<String, Serializable>();

	public Contents() {
		super();
	}

	public boolean containsKey(String key) {
		return contents.containsKey(key);
	}

	public void set(String key, Serializable value) {
		contents.put(key, value);
	}

	public Object get(String key) {
		return contents.get(key);
	}

	private void readObject(ObjectInputStream inStream)
			throws ClassNotFoundException, IOException {
		inStream.defaultReadObject();
	}

	private void writeObject(ObjectOutputStream outStream) throws IOException {
		outStream.defaultWriteObject();
	}
}
