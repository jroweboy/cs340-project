package phase4.classes;

import junit.framework.TestCase;
import dbPhase.hypeerweb.HyPeerWeb;
import dbPhase.hypeerweb.Node;

public class BlackBoxTesting extends TestCase {

	public HyPeerWeb hypeerweb = null;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		hypeerweb = HyPeerWeb.getSingleton();
	}

	public void Test_Equivalence_Partitioning() {
		// Valid
		// Adding to Hypeerweb
		hypeerweb.addToHyPeerWeb(new Node(0));
		hypeerweb.addToHyPeerWeb(new Node(1));
		hypeerweb.addToHyPeerWeb(new Node(2));

		hypeerweb.addToHyPeerWeb(new Node(3));

		assertEquals(hypeerweb.size(), 4);

		// Removing from Hypeerweb
		Node node = new Node(4);
		hypeerweb.addToHyPeerWeb(node);

		hypeerweb.removeNode(node);
		assertEquals(hypeerweb.size(), 4);

		// Removing from the middle
		hypeerweb.removeNode(new Node(1));
		assertEquals(hypeerweb.size(), 3);
		// Removing from the beginning
		hypeerweb.removeNode(new Node(0));
		assertEquals(hypeerweb.size(), 2);

		// Invalid
		// Add nothing
		hypeerweb.addToHyPeerWeb(null);
		// Add a node that already exists
		hypeerweb.addToHyPeerWeb(new Node(0));
		// Add a node whose webid is out of reange
		hypeerweb.addToHyPeerWeb(new Node(25));
	}
}
