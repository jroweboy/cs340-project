package phase4.classes;

import junit.framework.TestCase;
import phase6.MachineAddress;
import phase6.PortNumber;
import dbPhase.hypeerweb.HyPeerWeb;
import dbPhase.hypeerweb.HyPeerWebDatabase;
import dbPhase.hypeerweb.Node;

public class RobertWhiteBoxTesting extends TestCase {

	HyPeerWeb hypeerweb;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		MachineAddress.setMachineAddress("127.0.1.1");
		PortNumber.setApplicationsPortNumber(new PortNumber(50505));
		hypeerweb = HyPeerWeb.getSingleton();
	}

	public void testLoopTestingSendVisitor() {
		// it is impossible to skip the loop

		// this block will go through the loop exactly once
		createHyPeerWeb(2);
		Node startNode = hypeerweb.getNode(0);
		TestSendVisitor visitor = new TestSendVisitor();
		Parameters params = SendVisitor.createInitialParameters(1);
		visitor.visit(startNode, params);

		// this block will go through the loop more than once
		createHyPeerWeb(8);
		startNode = hypeerweb.getNode(7);
		params = SendVisitor.createInitialParameters(1);
		visitor.visit(startNode, params);
	}

	public void testRelationalAndBoundaryTestingSendVisitor() {
		// for the first condition we want to check target less than
		// biggest,equal to biggest, and greater than biggest.
		createHyPeerWeb(8);
		Node startNode = hypeerweb.getNode(1);
		TestSendVisitor visitor = new TestSendVisitor();
		Parameters params = SendVisitor.createInitialParameters(7);
		visitor.visit(startNode, params);
		params = SendVisitor.createInitialParameters(9);
		visitor.visit(startNode, params);
		params = SendVisitor.createInitialParameters(8);
		visitor.visit(startNode, params);

		// for the second condition we want to check lest than,equal to, and
		// greater than
		startNode = hypeerweb.getNode(4);
		params = SendVisitor.createInitialParameters(4);
		visitor.visit(startNode, params);
		params = SendVisitor.createInitialParameters(5);
		visitor.visit(startNode, params);
		params = SendVisitor.createInitialParameters(3);
		visitor.visit(startNode, params);

	}

	public void testCompleteCoverageTestingSendVisitor() {
		createHyPeerWeb(2);
		Node startNode = hypeerweb.getNode(0);
		TestSendVisitor visitor = new TestSendVisitor();
		Parameters params = new Parameters();
		visitor.visit(startNode, params);
		params = SendVisitor.createInitialParameters(0);
		visitor.visit(startNode, params);
		params = SendVisitor.createInitialParameters(-1);
		params.set("target", -1);
		visitor.visit(startNode, params);
		params = SendVisitor.createInitialParameters(2);
		visitor.visit(startNode, params);
		params = SendVisitor.createInitialParameters(1);
		visitor.visit(startNode, params);
		createHyPeerWeb(64);
		params = SendVisitor.createInitialParameters(23);
		startNode = hypeerweb.getNode(0);
		visitor.visit(startNode, params);
		params = SendVisitor.createInitialParameters(16);
		visitor.visit(startNode, params);
		params = SendVisitor.createInitialParameters(43);
		visitor.visit(startNode, params);
		startNode = hypeerweb.getNode(43);
		params = SendVisitor.createInitialParameters(12);
		visitor.visit(startNode, params);
	}

	public void testDataFlowTestingSendVisitor() {
		// checking to see if the parameter has been modified
		createHyPeerWeb(2);
		Node startNode = hypeerweb.getNode(0);
		TestSendVisitor visitor = new TestSendVisitor();
		Parameters params = new Parameters();
		visitor.visit(startNode, params);
	}

	private void createHyPeerWeb(int numberOfNodes) {
		hypeerweb.clear();
		HyPeerWebDatabase.getSingleton().clear();

		for (int i = 0; i < numberOfNodes; i++) {
			Node node = new Node(i);
			hypeerweb.addToHyPeerWeb(node);
		}
	}

	private class TestSendVisitor extends SendVisitor {

		/**
		 * 
		 */
		private static final long serialVersionUID = -1410121627118828336L;

		@Override
		public void targetOperation(Node node, Parameters parameters) {
			System.out.println("Reached target node: "
					+ parameters.get("target"));
			if (parameters.get("testing").equals("testing")) {
				System.out.println("Parameter testing was not modified.");
			} else {
				System.err.println("Parameter testing was modified.");
			}
		}

		@Override
		public void intermediateOperation(Node node, Parameters params) {
			if (params.get("testing").equals("testing")) {
				System.out.println("Parameter testing was not modified.");
			} else {
				System.err.println("Parameter testing was modified.");
			}
		}

	}

}
