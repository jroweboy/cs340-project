package phase4.classes;

import dbPhase.hypeerweb.Node;
import dbPhase.hypeerweb.WebId;

/**
 * The abstract class that Visitors must override in order to broadcast to all
 * the nodes <br>
 * <br>
 * 
 * <pre>
 * <b>Domain:</b>
 *     NONE
 * <b>Invariants</b>
 *     NONE
 * </pre>
 * 
 * @author James Rowe
 */
public abstract class BroadcastVisitor implements Visitor {

	/**
	 * Default constructor.
	 */
	public BroadcastVisitor() {
		super();
	}

	/**
	 * Creates the minimum set of parameters needed when invoking an accept
	 * method during a broadcast. The one minimum requirement is the parameters
	 * are not able to be reused on separate broadcasts. If there are more
	 * required parameters in a subclass, this method is overridden.
	 * 
	 * @return Parameters that contain a single key indicating that the
	 *         broadcast should start from nodeZero. If this is set to true then
	 *         the algorithm will fail.
	 * 
	 * @author Thomas Holladay and James Rowe
	 */
	public static Parameters createInitialParameters() {
		Parameters p = new Parameters();
		p.set("zeroVisited", Boolean.FALSE);
		return p;
	}

	@Override
	/**
	 * The algorithm that will visit each of the nodes. This relies on starting 
	 * from node Zero and to detect that a flag was set in parameters. Parameters
	 * either needs to not set that flag or to leave it as false in order to guarantee
	 * that all the nodes are visited.
	 * 
	 * @param node - The node that we will visit along with all its neighbors one off from it
	 * @param parameters - 
	 * 
	 * @pre parameters was created through the createInitialParameters methods and 
	 * 		 the key zeroVisited is either non existent or set to Boolean.FALSE
	 * @post this node and all the next neighbors to visit have had <b>operation(Node, Parameters)</b> called on it 
	 * 
	 * @author Thomas Holladay and James Rowe
	 */
	public void visit(Node node, Parameters parameters) {
		if (!parameters.containsKey("zeroVisited")
				|| parameters.get("zeroVisited").equals(Boolean.FALSE)) {
			parameters.set("zeroVisited", Boolean.TRUE);
			node.findZero().accept(this, parameters);
			return;
		}
		if (node.getValue() == 0) {
			operation(node, parameters);
			for (Node n : node.getNeighbors()) {
				visit(n, parameters);
			}
		} else {
			operation(node, parameters);
			for (Node n : node.getNeighbors()) {
				if (WebId.getLowestBitPos(n.getValue()) < WebId
						.getLowestBitPos(node.getValue())) {
					visit(n, parameters);
				}
			}
		}
	}

	/**
	 * Abstract method that will be called on each node visited. This method
	 * must be implemented in subclasses in order to produce results If you only
	 * need to communicate with one node then use a SendVisitor instead
	 * 
	 * @param node
	 *            - This is the node that we are sending a broadcast to
	 * @param parameters
	 *            - the parameters that were passed in originally
	 * 
	 * @pre none
	 * @post This operation was called on the node with the given parameters
	 */
	protected abstract void operation(Node node, Parameters parameters);
}
