package phase4.classes;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Parameters implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1614139680986451472L;
	Map<String, Object> parameters = new HashMap<String, Object>();

	public boolean containsKey(String key) {
		return parameters.containsKey(key);
	}

	public void set(String key, Object value) {
		parameters.put(key, value);
	}

	public Object get(String key) {
		return parameters.get(key);
	}

}
