package phase4.classes;

import java.util.ArrayList;

import junit.framework.TestCase;
import dbPhase.hypeerweb.HyPeerWeb;
import dbPhase.hypeerweb.HyPeerWebDatabase;
import dbPhase.hypeerweb.Node;

public class JamesTesting extends TestCase {

	private class MyBroadcastVisitor extends BroadcastVisitor {
		@Override
		protected void operation(Node node, Parameters parameters) {
			((StringBuilder) parameters.get("printer")).append(" "
					+ node.getValue());
		}
	}

	public HyPeerWeb hypeerweb = null;

	private void createHyPeerWeb(int numberOfNodes) {
		hypeerweb.clear();
		HyPeerWebDatabase.getSingleton().clear();

		for (int i = 0; i < numberOfNodes; i++) {
			Node node = new Node(i);
			hypeerweb.addToHyPeerWeb(node);
		}
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		hypeerweb = HyPeerWeb.getSingleton();
	}

	public void testBoundaryValue() {
		// Test the boundaries when you broadcast on one smaller than
		// a full hypeerweb then a perfect hypeercube, and last
		// test on a one bigger than perfect hypeercube.
		MyBroadcastVisitor m = new MyBroadcastVisitor();

		System.out.println("Testing size 14");
		createHyPeerWeb(14);
		Parameters p = BroadcastVisitor.createInitialParameters();
		StringBuilder a = new StringBuilder();
		p.set("printer", a);
		hypeerweb.getNode(0).accept(m, p);
		System.out.println(a.toString());

		System.out.println("Testing size 15");
		createHyPeerWeb(15);
		a = new StringBuilder();
		p.set("printer", a);
		hypeerweb.getNode(0).accept(m, p);
		System.out.println(a.toString());

		System.out.println("Testing size 16");
		createHyPeerWeb(16);
		a = new StringBuilder();
		p.set("printer", a);
		hypeerweb.getNode(0).accept(m, p);
		System.out.println(a.toString());
	}

	public void testDataFlow() {
		// Test to see if the parameters get changed when I make my own
		// parameters and when I use the default ones
		MyBroadcastVisitor m = new MyBroadcastVisitor();
		Parameters p; // = MyBroadcastVisitor.createInitialParameters();
		StringBuilder a; // = new StringBuilder();
		StringBuilder missing;
		String[] t;
		ArrayList<String> has;

		System.out.println("dataFlowTesting parameters default");
		p = BroadcastVisitor.createInitialParameters();
		a = new StringBuilder();
		p.set("printer", a);
		createHyPeerWeb(32);
		hypeerweb.getNode(0).accept(m, p);
		// validate the output to see if every node was visited
		missing = new StringBuilder("Missing ");
		has = new ArrayList<String>();
		t = a.toString().split(" ");
		for (int i = 0; i < t.length; ++i) {
			has.add(t[i]);
		}
		for (int i = 0; i < 32; ++i) {
			if (!has.contains("" + i)) {
				missing.append(" " + i);
			}
		}
		if (missing.toString().equals("Missing ")) {
			missing.append("None");
		}

		System.out.println(missing.toString());

		System.out.println("dataFlowTesting parameters with no zeroVisited");
		p = new Parameters();
		a = new StringBuilder();
		p.set("printer", a);
		createHyPeerWeb(32);
		hypeerweb.getNode(0).accept(m, p);
		// validate the output to see if every node was visited
		missing = new StringBuilder("Missing ");
		has = new ArrayList<String>();
		t = a.toString().split(" ");
		for (int i = 0; i < t.length; ++i) {
			has.add(t[i]);
		}
		for (int i = 0; i < 32; ++i) {
			if (!has.contains("" + i)) {
				missing.append(" " + i);
			}
		}
		if (missing.toString().equals("Missing ")) {
			missing.append("None");
		}

		System.out.println(missing.toString());

	}

	// ////////////////////////
	//
	// Begin black box testing
	//

	public void testEquivalence() {

		MyBroadcastVisitor m = new MyBroadcastVisitor();
		Parameters p = BroadcastVisitor.createInitialParameters();
		StringBuilder a = new StringBuilder();

		p.set("printer", a);

		System.out.println("Invalid: Broadcast from Node -1");
		createHyPeerWeb(16);
		hypeerweb.getNode(-1).accept(m, p);
		System.out.println(a.toString());

		System.out.println("Invalid: Broadcast from Node 16");
		a = new StringBuilder();
		p = BroadcastVisitor.createInitialParameters();
		p.set("printer", a);
		hypeerweb.getNode(16).accept(m, p);
		System.out.println(a.toString());

		System.out.println("Valid: Broadcast from Node 1");
		a = new StringBuilder();
		p = BroadcastVisitor.createInitialParameters();
		p.set("printer", a);
		hypeerweb.getNode(1).accept(m, p);
		System.out.println(a.toString());

		System.out.println("Valid: Broadcast from Node 3");
		a = new StringBuilder();
		p = BroadcastVisitor.createInitialParameters();
		p.set("printer", a);
		hypeerweb.getNode(1).accept(m, p);
		System.out.println(a.toString());

		// start at the largest node and broadcast
		System.out.println("Valid: Broadcast from Node 15");
		p = BroadcastVisitor.createInitialParameters();
		a = new StringBuilder();
		p.set("printer", a);
		hypeerweb.getNode(15).accept(m, p);
		System.out.println(a.toString());
	}

	public void testInternalBoundaryValue() {
		// Test to see if the broadcast visitor will visit each node only once
		MyBroadcastVisitor m = new MyBroadcastVisitor();
		Parameters p;
		StringBuilder a;
		StringBuilder missing;
		String[] t;
		ArrayList<String> has;

		System.out.println("internal Boundary Testing");
		p = BroadcastVisitor.createInitialParameters();
		a = new StringBuilder();
		p.set("printer", a);
		createHyPeerWeb(32);
		hypeerweb.getNode(0).accept(m, p);
		// validate the output to see if every node was visited
		missing = new StringBuilder("Missing ");
		has = new ArrayList<String>();
		t = a.toString().split(" ");
		for (int i = 0; i < t.length; ++i) {
			has.add(t[i]);
		}
		for (int i = 0; i < 32; ++i) {
			if (!has.contains("" + i)) {
				missing.append(" " + i);
			}
		}
		if (missing.toString().equals("Missing ")) {
			missing.append("None");
		}

		System.out.println(missing.toString());
	}

	public void testLoop() {
		// Test to see if the broadcast visitor will visit each node only once
		MyBroadcastVisitor m = new MyBroadcastVisitor();
		Parameters p; // = MyBroadcastVisitor.createInitialParameters();
		StringBuilder a; // = new StringBuilder();
		StringBuilder missing;
		String[] t;
		ArrayList<String> has;

		System.out.println("loopTesting size 32");
		p = BroadcastVisitor.createInitialParameters();
		a = new StringBuilder();
		p.set("printer", a);
		createHyPeerWeb(32);
		hypeerweb.getNode(0).accept(m, p);
		// validate the output to see if every node was visited
		missing = new StringBuilder("Missing ");
		has = new ArrayList<String>();
		t = a.toString().split(" ");
		for (int i = 0; i < t.length; ++i) {
			has.add(t[i]);
		}
		for (int i = 0; i < 32; ++i) {
			if (!has.contains("" + i)) {
				missing.append(" " + i);
			}
		}
		if (missing.toString().equals("Missing ")) {
			missing.append("None");
		}

		System.out.println(missing.toString());

		// do the same thing as above except with 64 nodes instead
		System.out.println("loopTesting size 64");
		p = BroadcastVisitor.createInitialParameters();
		a = new StringBuilder();
		p.set("printer", a);
		createHyPeerWeb(64);
		hypeerweb.getNode(0).accept(m, p);
		missing = new StringBuilder("Missing ");
		has = new ArrayList<String>();
		t = a.toString().split(" ");
		for (int i = 0; i < t.length; ++i) {
			has.add(t[i]);
		}
		for (int i = 0; i < 64; ++i) {
			if (!has.contains("" + i)) {
				missing.append(" " + i);
			}
		}
		if (missing.toString().equals("Missing ")) {
			missing.append("None");
		}
		System.out.println(missing.toString());

		// do the same thing as above except with 0 nodes instead
		System.out.println("loopTesting size 0");
		p = BroadcastVisitor.createInitialParameters();
		a = new StringBuilder();
		p.set("printer", a);
		createHyPeerWeb(1);
		hypeerweb.getNode(0).accept(m, p);
		// validate the output to see if every node was visited
		missing = new StringBuilder("Missing ");
		has = new ArrayList<String>();
		t = a.toString().split(" ");
		for (int i = 0; i < t.length; ++i) {
			has.add(t[i]);
		}
		for (int i = 0; i < 1; ++i) {
			if (!has.contains("" + i)) {
				missing.append(" " + i);
			}
		}
		if (missing.toString().equals("Missing ")) {
			missing.append("None");
		}
		System.out.println(missing.toString());
	}

	public void testRelational() {
		//
		MyBroadcastVisitor m = new MyBroadcastVisitor();
		Parameters p; // = MyBroadcastVisitor.createInitialParameters();
		StringBuilder a; // = new StringBuilder();
		StringBuilder missing;
		String[] t;
		ArrayList<String> has;

		System.out.println("relationalTesting ");
		p = BroadcastVisitor.createInitialParameters();
		a = new StringBuilder();
		p.set("printer", a);
		createHyPeerWeb(32);
		hypeerweb.getNode(0).accept(m, p);
		// validate the output to see if every node was visited
		missing = new StringBuilder("Missing ");
		has = new ArrayList<String>();
		t = a.toString().split(" ");
		for (int i = 0; i < t.length; ++i) {
			has.add(t[i]);
		}
		for (int i = 0; i < 32; ++i) {
			if (!has.contains("" + i)) {
				missing.append(" " + i);
			}
		}
		if (missing.toString().equals("Missing ")) {
			missing.append("None");
		}

		System.out.println(missing.toString());
	}

}