package phase4.classes;

import junit.framework.TestCase;
import dbPhase.hypeerweb.HyPeerWeb;
import dbPhase.hypeerweb.HyPeerWebDatabase;
import dbPhase.hypeerweb.Node;

public class Equivalence_Partitioning extends TestCase {

	public HyPeerWeb hypeerweb = null;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testEquivalencePartitioning() {
		// VALID
		hypeerweb = HyPeerWeb.getSingleton();

		// When size is one
		createHyPeerWebWith(1);
		Parameters parameters = BroadcastVisitor.createInitialParameters();
		Node n = hypeerweb.getNode(0);
		n.accept(new MyBroadcastVisitor(), parameters);

		parameters = SendVisitor.createInitialParameters(0);
		n.accept(new MySendVisitor(), parameters);

		// When size is Perfect hypeercube
		createHyPeerWebWith(8);
		parameters = BroadcastVisitor.createInitialParameters();
		n = hypeerweb.getNode(0);
		n.accept(new MyBroadcastVisitor(), parameters);

		// When target is 0
		parameters = SendVisitor.createInitialParameters(0);
		n.accept(new MySendVisitor(), parameters);

		// When target is a cap node
		parameters = SendVisitor.createInitialParameters(7);
		n.accept(new MySendVisitor(), parameters);

		// When target is not the first or the cap node
		parameters = SendVisitor.createInitialParameters(5);
		n.accept(new MySendVisitor(), parameters);

		// When size is not a Perfect hypeercube
		createHyPeerWebWith(10);
		parameters = BroadcastVisitor.createInitialParameters();
		n = hypeerweb.getNode(0);
		n.accept(new MyBroadcastVisitor(), parameters);

		// When target is 0
		parameters = SendVisitor.createInitialParameters(0);
		n.accept(new MySendVisitor(), parameters);

		// When target is the last node
		parameters = SendVisitor.createInitialParameters(9);
		n.accept(new MySendVisitor(), parameters);

		// When target is not the first or the cap node
		parameters = SendVisitor.createInitialParameters(5);
		n.accept(new MySendVisitor(), parameters);

		// When target is what used to be the cap node
		parameters = SendVisitor.createInitialParameters(7);
		n.accept(new MySendVisitor(), parameters);

		// INVALID

		// When the size is zero
		createHyPeerWebWith(0);
		parameters = BroadcastVisitor.createInitialParameters();
		n = hypeerweb.getNode(0);
		n.accept(new MyBroadcastVisitor(), parameters);

		parameters = SendVisitor.createInitialParameters(0);
		n.accept(new MySendVisitor(), parameters);

		// When the size is greater than zero but the target given is negative
		createHyPeerWebWith(10);

		parameters = SendVisitor.createInitialParameters(-2);
		n.accept(new MySendVisitor(), parameters);

		// When the size is greater than zero but the target is not inside of
		// the hypeerweb

		parameters = SendVisitor.createInitialParameters(20);
		n.accept(new MySendVisitor(), parameters);
	}

	private void createHyPeerWebWith(int numberOfNodes) {
		hypeerweb.clear();
		HyPeerWebDatabase.getSingleton().clear();
		Node node0 = new Node(0);
		hypeerweb.addToHyPeerWeb(node0, null);

		for (int i = 1; i < numberOfNodes; i++) {
			Node node = new Node(0);
			node0.addToHyPeerWeb(node);
		}
	}

}

//
//
// import hypeerweb.database.HyPeerWebDatabase;
// import hypeerweb.hypeerweb.BroadcastVisitor;
// import hypeerweb.hypeerweb.HyPeerWeb;
// import hypeerweb.hypeerweb.NodeImplementation;
// import hypeerweb.hypeerweb.Parameters;
// import hypeerweb.hypeerweb.SendVisitor;
// import hypeerweb.hypeerweb.proxies.Node;
// import junit.framework.TestCase;
// //import dbPhase.hypeerweb.HyPeerWeb;
// //import dbPhase.hypeerweb.HyPeerWebDatabase;
// ///import dbPhase.hypeerweb.Node;
//
// public class Equivalence_Partitioning extends TestCase {
//
// public HyPeerWeb hypeerweb = null;
//
// protected void setUp() throws Exception {
// super.setUp();
// }
//
// public void testEquivalencePartitioning(){
// //VALID
// hypeerweb = HyPeerWeb.getSingleton();
//
// //When size is one
// createHyPeerWebWith(1);
// Parameters parameters = BroadcastVisitor.createInitialParameters();
// Node n = hypeerweb.getNode(0);
// n.accept(new MyBroadcastVisitor(), parameters);
//
// parameters = SendVisitor.createInitialParameters(0);
// n.accept(new MySendVisitor(), parameters);
//
// //When size is Perfect hypeercube
// createHyPeerWebWith(8);
// parameters = BroadcastVisitor.createInitialParameters();
// n = hypeerweb.getNode(0);
// n.accept(new MyBroadcastVisitor(), parameters);
//
// //When target is 0
// parameters = SendVisitor.createInitialParameters(0);
// n.accept(new MySendVisitor(), parameters);
//
// //When target is a cap node
// parameters = SendVisitor.createInitialParameters(7);
// n.accept(new MySendVisitor(), parameters);
//
// //When target is not the first or the cap node
// parameters = SendVisitor.createInitialParameters(5);
// n.accept(new MySendVisitor(), parameters);
//
// //When size is not a Perfect hypeercube
// createHyPeerWebWith(10);
// parameters = BroadcastVisitor.createInitialParameters();
// n = hypeerweb.getNode(0);
// n.accept(new MyBroadcastVisitor(), parameters);
//
// //When target is 0
// parameters = SendVisitor.createInitialParameters(0);
// n.accept(new MySendVisitor(), parameters);
//
// //When target is the last node
// parameters = SendVisitor.createInitialParameters(9);
// n.accept(new MySendVisitor(), parameters);
//
// //When target is not the first or the cap node
// parameters = SendVisitor.createInitialParameters(5);
// n.accept(new MySendVisitor(), parameters);
//
// //When target is what used to be the cap node
// parameters = SendVisitor.createInitialParameters(7);
// n.accept(new MySendVisitor(), parameters);
//
// System.out.println("Starting the Invalids");
// //INVALID
// try{
// //When the size is zero
// createHyPeerWebWith(0);
// parameters = BroadcastVisitor.createInitialParameters();
// n = hypeerweb.getNode(0);
// n.accept(new MyBroadcastVisitor(), parameters);
//
// parameters = SendVisitor.createInitialParameters(0);
// n.accept(new MySendVisitor(), parameters);
//
// //When the size is greater than zero but the target given is negative
// createHyPeerWebWith(10);
//
// parameters = SendVisitor.createInitialParameters(-2);
// n.accept(new MySendVisitor(), parameters);
//
// //When the size is greater than zero but the target is not inside of the
// hypeerweb
//
// parameters = SendVisitor.createInitialParameters(20);
// n.accept(new MySendVisitor(), parameters);
// }catch(Exception e)
// {
// System.out.println("Need to fix this. "+e);
// }
// }
//
// private void createHyPeerWebWith(int numberOfNodes) {
// hypeerweb.clear();
// HyPeerWebDatabase.getSingleton().clear();
// NodeImplementation node0 = new NodeImplementation(0);
// hypeerweb.addToHyPeerWeb(node0, null);
//
// for (int i = 1; i < numberOfNodes; i++) {
// NodeImplementation node = new NodeImplementation(0);
// node0.addToHyPeerWeb(node);
// }
// }
//
// class MySendVisitor extends SendVisitor {
//
// @Override
// protected void targetOperation(Node node, Parameters parameters) {
// System.out.println("WE GOT TO TARGET " + node + " with start node "
// + parameters.get("start"));
//
// }
//
// @Override
// protected void intermediateOperation(Node node, Parameters parameters) {
// System.out.println("WE ARE AT " + node + " GOING TO NODE "
// + parameters.get("target"));
// }
//
// }
//
// class MyBroadcastVisitor extends BroadcastVisitor {
//
// @Override
// protected void operation(Node node, Parameters parameters) {
// System.out.println("Broadcasting from " + node);
// }
//
//
// }
//
//
// }
//
//
