package phase4.classes;

import junit.framework.TestCase;
import dbPhase.hypeerweb.HyPeerWeb;
import dbPhase.hypeerweb.HyPeerWebDatabase;
import dbPhase.hypeerweb.Node;
import dbPhase.hypeerweb.WebId;

/**
 * @author Thomas Holladay
 */
public class WhiteBoxTesting extends TestCase {

	public HyPeerWeb hypeerweb = null;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		hypeerweb = HyPeerWeb.getSingleton();
	}

	public void getHypeerWebSize(int numberOfNodes) {
		HyPeerWebDatabase.getSingleton().clear();
		hypeerweb.clear();
		Node node0 = new Node(0);
		hypeerweb.addToHyPeerWeb(node0, null);

		for (int i = 1; i < numberOfNodes; i++) {
			Node node = new Node(0);
			node0.addToHyPeerWeb(node);
		}
	}

	public void testHyPeerWeb() {
		testLoopTestingHyPeerWeb();
		testRelationalTestingHyPeerWeb();
		testInternalBoundaryValueTestingHyPeerWeb();
		testDataFlowTestingHyPeerWeb();
	}

	public void testLoopTestingHyPeerWeb() {

		hypeerweb.clear();
		HyPeerWebDatabase.getSingleton().clear();
		// Testing to see if it loops correctly when adding to an empty
		// hypeerweb
		Node zero = new Node(0);
		hypeerweb.addToHyPeerWeb(zero);
		assertEquals(1, hypeerweb.size());
		assertEquals(true, hypeerweb.contains(zero));

		createHyPeerWeb(5);
		Node five = new Node(5);
		hypeerweb.addToHyPeerWeb(five);

		// Test to see if it loops correctly when adding to a filled hypeerweb
		assertEquals(6, hypeerweb.size());
		// assertEquals(true, hypeerweb.contains(five));

	}

	public void testRelationalTestingHyPeerWeb() {
		createHyPeerWeb(0);

		// Test when HypeerWeb is empty
		hypeerweb.addToHyPeerWeb(new Node(0));

		// Test when adding a Node to a Hypeerweb that isn't empty
		hypeerweb.addToHyPeerWeb(new Node(1));

		createHyPeerWeb(0);

		// Test contains when Hypeerweb is empty
		assertEquals(false, hypeerweb.contains(new Node(0)));

		hypeerweb.addToHyPeerWeb(new Node(0));
		// Test when contains a node and is the first node
		assertEquals(true, hypeerweb.contains(new Node(0)));
		// Test when contains a node but isn't the first node
		assertEquals(false, hypeerweb.contains(new Node(1)));

		// Test when getNode wants the zeroNode
		assertEquals(0, hypeerweb.getNode(0).getWebId().getValue());

	}

	public void testInternalBoundaryValueTestingHyPeerWeb() {

		createHyPeerWeb(5);

		// Test that the highest webId isn't greater than the size
		assertTrue(hypeerweb.getNode(4).getWebId().getValue() < hypeerweb
				.size());

	}

	public void testDataFlowTestingHyPeerWeb() {

		createHyPeerWeb(6);

		// Test to make sure that when a hypeerweb is made, the zeroNode has
		// webId of zero
		Node six = new Node(6);
		hypeerweb.addToHyPeerWeb(six);

		assertEquals(0, hypeerweb.getNode(0).getWebId().getValue());

		// Test to make that when node 1 is removed from the Hypeerweb that new
		// node 1 has value of Node 5
		Object value = hypeerweb.getNode(5).getValue();
		hypeerweb.removeNode(six);

		assertEquals(value, hypeerweb.getNode(5).getValue());

		hypeerweb.addToHyPeerWeb(six);
		hypeerweb.addToHyPeerWeb(new Node(7));
		hypeerweb.addToHyPeerWeb(new Node(8));

		// Test when removing the zero node
		createHyPeerWeb(0);
		Node zero = new Node(0);
		Node one = new Node(1);
		Node two = new Node(2);

		hypeerweb.addToHyPeerWeb(zero);
		hypeerweb.addToHyPeerWeb(one);
		hypeerweb.addToHyPeerWeb(two);
		hypeerweb.removeNode(zero);

		assertEquals(false, hypeerweb.contains(new Node(2)));
		assertEquals(true, hypeerweb.contains(new Node(0)));

	}

	public void testWebId() {
		testLoopTestingWebId();
		testRelationalTestingWebId();
		testInternalBoundaryValueTestingWebId();
		testDataFlowTestingWebId();
	}

	private void testLoopTestingWebId() {
		// we basically have no looping structures...
	}

	private void testRelationalTestingWebId() {

	}

	private void testInternalBoundaryValueTestingWebId() {

	}

	private void testDataFlowTestingWebId() {

	}

	private void createHyPeerWeb(int numberOfNodes) {
		hypeerweb.clear();
		HyPeerWebDatabase.getSingleton().clear();

		for (int i = 0; i < numberOfNodes; i++) {
			Node node = new Node(i);
			hypeerweb.addToHyPeerWeb(node);
		}
	}

	public void testNullNode() {
		// go through all of the methods of Null Node and make sure they don't
		// make the program crash.

		Node n = Node.NULL_NODE;
		Node z = new Node(0);

		n.addToHyPeerWeb(z);
		n.addDownPointer(z);
		n.addNeighbor(z);
		n.addUpPointer(z);

		n.setFold(z);
		n.setInverseSurrogateFold(z);
		n.setSurrogateFold(z);
		n.setWebId(new WebId(0));

		n.replace(z, z);
		n.constructSimplifiedNodeDomain();
		n.equals(n);
		n.equals(z);

		n.removeDownPointer(z);
		n.removeNeighbor(z);
		n.removeUpPointer(z);
	}

	public void testConstructors() {
		Node n = new Node(new WebId(0));
		Node m = new Node(1, 2);
		n.toString();
		n.clearConnections();
		n.initializeChild(m);
	}

	public void testAddingNodesToHyPeerWeb() {
		Node one = new Node(0);
		Node two = new Node(1);
		Node three = new Node(2);

		hypeerweb.addToHyPeerWeb(one);
		hypeerweb.addToHyPeerWeb(two);
		hypeerweb.addToHyPeerWeb(three, two);
		two.ourToString();
		one.ourToString();
		three.ourToString();

		hypeerweb.removeNode(three);

		two.ourToString();

		hypeerweb.addToHyPeerWeb(three);
		one.replace(two, three);
	}
}
