package phase4.classes;

import dbPhase.hypeerweb.Node;

public interface Visitor {

	void visit(Node node, Parameters parameters);

}
