package phase4.classes;

import java.util.ArrayList;

import junit.framework.TestCase;
import dbPhase.hypeerweb.HyPeerWeb;
import dbPhase.hypeerweb.HyPeerWebDatabase;
import dbPhase.hypeerweb.Node;

/**
 * @author Thomas Holladay
 */
public class VisitorTest extends TestCase {

	private HyPeerWeb hypeerweb;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		hypeerweb = HyPeerWeb.getSingleton();
	}

	public void testSendVisitorForBoundaryValues() {
		ArrayList<ArrayList<Integer>> starts = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		sizes.add(1);
		sizes.add(2);
		sizes.add(3);
		sizes.add(4);
		sizes.add(6);
		sizes.add(127);
		sizes.add(128);
		for (int i = 0; i < sizes.size(); i++) {
			starts.add(i, new ArrayList<Integer>());
		}
		starts.get(0).add(0);

		starts.get(1).add(0);
		starts.get(1).add(1);

		starts.get(2).add(0);
		starts.get(2).add(1);
		starts.get(2).add(2);

		starts.get(3).add(0);
		starts.get(3).add(1);
		starts.get(3).add(2);
		starts.get(3).add(3);

		starts.get(4).add(0);
		starts.get(4).add(1);
		starts.get(4).add(2);
		starts.get(4).add(3);
		starts.get(4).add(4);
		starts.get(4).add(5);

		starts.get(5).add(0);
		starts.get(5).add(1);
		starts.get(5).add(2);
		starts.get(5).add(3);
		starts.get(5).add(4);
		starts.get(5).add(62);
		starts.get(5).add(63);
		starts.get(5).add(64);
		starts.get(5).add(125);
		starts.get(5).add(126);

		starts.get(6).add(0);
		starts.get(6).add(1);
		starts.get(6).add(2);
		starts.get(6).add(3);
		starts.get(6).add(4);
		starts.get(6).add(62);
		starts.get(6).add(63);
		starts.get(6).add(64);
		starts.get(6).add(126);
		starts.get(6).add(127);

		Parameters params;

		for (int i = 0; i < sizes.size(); i++) {
			System.out.println("Current Size: " + sizes.get(i));
			ArrayList<Integer> current = starts.get(i);
			getHypeerWebSize(sizes.get(i));
			Node n;
			for (int j = 0; j < current.size(); j++) {
				params = SendVisitor.createInitialParameters(current.get(j));
				for (int k = 0; k < current.size(); k++) {
					n = hypeerweb.getNode(current.get(k));
					params.set("start", new Integer(n.getWebId().getValue()));
					n.accept(new MySendVisitor(), params);
				}
			}
			if (sizes.get(i) == 127 || sizes.get(i) == 128) {
				n = hypeerweb.getNode(3);
				params = SendVisitor.createInitialParameters(124);
				params.set("start", new Integer(n.getWebId().getValue()));
				n.accept(new MySendVisitor(), params);

				n = hypeerweb.getNode(4);
				params = SendVisitor.createInitialParameters(123);
				params.set("start", new Integer(n.getWebId().getValue()));
				n.accept(new MySendVisitor(), params);

				n = hypeerweb.getNode(125);
				params = SendVisitor.createInitialParameters(93);
				params.set("start", new Integer(n.getWebId().getValue()));
				n.accept(new MySendVisitor(), params);
			}
		}
	}

	public void testBroadCastVisitorForBoundaryValues() {
		ArrayList<ArrayList<Integer>> starts = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> sizes = new ArrayList<Integer>();
		sizes.add(1);
		sizes.add(2);
		sizes.add(3);
		sizes.add(4);
		sizes.add(6);
		sizes.add(127);
		sizes.add(128);
		for (int i = 0; i < sizes.size(); i++) {
			starts.add(i, new ArrayList<Integer>());
		}
		starts.get(0).add(0);

		starts.get(1).add(0);
		starts.get(1).add(1);

		starts.get(2).add(0);
		starts.get(2).add(1);
		starts.get(2).add(2);

		starts.get(3).add(0);
		starts.get(3).add(1);
		starts.get(3).add(2);
		starts.get(3).add(3);

		starts.get(4).add(0);
		starts.get(4).add(1);
		starts.get(4).add(2);
		starts.get(4).add(3);
		starts.get(4).add(4);
		starts.get(4).add(5);

		starts.get(5).add(0);
		starts.get(5).add(1);
		starts.get(5).add(2);
		starts.get(5).add(3);
		starts.get(5).add(4);
		starts.get(5).add(62);
		starts.get(5).add(63);
		starts.get(5).add(64);
		starts.get(5).add(125);
		starts.get(5).add(126);

		starts.get(6).add(0);
		starts.get(6).add(1);
		starts.get(6).add(2);
		starts.get(6).add(3);
		starts.get(6).add(4);
		starts.get(6).add(62);
		starts.get(6).add(63);
		starts.get(6).add(64);
		starts.get(6).add(126);
		starts.get(6).add(127);

		Parameters params;

		for (int i = 0; i < sizes.size(); i++) {
			if (sizes.get(i) < 100) {
				continue;
			}
			System.out.println("Current Size: " + sizes.get(i));
			ArrayList<Integer> current = starts.get(i);
			getHypeerWebSize(sizes.get(i));
			Node n;
			for (int j = 0; j < current.size(); j++) {
				System.out.println("Starting at: " + j);
				params = BroadcastVisitor.createInitialParameters();
				n = hypeerweb.getNode(current.get(j));
				params.set("start", new Integer(n.getWebId().getValue()));
				n.accept(new MyBroadcastVisitor(), params);
			}
			if (sizes.get(i) == 127 || sizes.get(i) == 128) {
				n = hypeerweb.getNode(3);
				params = BroadcastVisitor.createInitialParameters();
				params.set("start", new Integer(n.getWebId().getValue()));
				n.accept(new MyBroadcastVisitor(), params);

				n = hypeerweb.getNode(4);
				params = BroadcastVisitor.createInitialParameters();
				params.set("start", new Integer(n.getWebId().getValue()));
				n.accept(new MyBroadcastVisitor(), params);

				n = hypeerweb.getNode(125);
				params = BroadcastVisitor.createInitialParameters();
				params.set("start", new Integer(n.getWebId().getValue()));
				n.accept(new MyBroadcastVisitor(), params);
			}
		}
	}

	private void getHypeerWebSize(int numberOfNodes) {
		HyPeerWebDatabase.getSingleton().clear();
		hypeerweb.clear();
		Node node0 = new Node(0);
		hypeerweb.addToHyPeerWeb(node0, null);

		for (int i = 1; i < numberOfNodes; i++) {
			Node node = new Node(0);
			node0.addToHyPeerWeb(node);
		}
	}

}

class MySendVisitor extends SendVisitor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4344078485059100678L;

	@Override
	protected void targetOperation(Node node, Parameters parameters) {
		System.out.println("WE GOT TO TARGET " + node + " with start node "
				+ parameters.get("start"));

	}

	@Override
	protected void intermediateOperation(Node node, Parameters parameters) {
		System.out.println("WE ARE AT " + node + " GOING TO NODE "
				+ parameters.get("target"));
	}

}

class MyBroadcastVisitor extends BroadcastVisitor {

	@Override
	protected void operation(Node node, Parameters parameters) {
		System.out.println("Broadcasting from " + node);
	}

}
