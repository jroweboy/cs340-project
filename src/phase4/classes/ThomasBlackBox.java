package phase4.classes;

import junit.framework.TestCase;
import dbPhase.hypeerweb.HyPeerWeb;
import dbPhase.hypeerweb.WebId;

public class ThomasBlackBox extends TestCase {
	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testWebId() {
		// Equivalence Partitioning

		WebId test = new WebId(0);
		WebId t = new WebId(1);
		t = new WebId(1, 1);

		// Valid Inputs

		WebId w = test.createChildsWebId();

		assertEquals(w.getValue(), 1);

		assertEquals(test.distanceTo(new WebId(3)), 2);
		t.distanceTo(test);
		assertEquals(test.equals(new WebId(0)), true);
		assertEquals(test.equals(test), true);
		assertEquals(test.equals(w), false);
		assertEquals(test.equals(new WebId(1, 10)), false);

		assertEquals(w.getHeight(), 1);
		assertEquals(test.getHeight(), 0);

		assertEquals(test.getValue(), 0);
		assertEquals(test.isCapNode(), true);
		assertEquals(w.isCapNode(), true);
		new WebId(10).isCapNode();
		assertEquals(WebId.NULL_WEB_ID.isCapNode(), false);

		test.setHeight(1);
		w.equals(w);
		assertEquals(test.isFoldOf(new WebId(1)), true);
		assertEquals(test.isFoldOf(new WebId(2)), false);

		assertEquals(test.isInverseSurrogateFoldOf(new WebId(1)), false);
		test.setHeight(2);
		assertEquals(test.isInverseSurrogateFoldOf(new WebId(1)), true);
		assertEquals(test.isNeighborOf(new WebId(1)), true);
		assertEquals(test.isNeighborOf(new WebId(2)), true);

		w.isNeighborOf(test);
		w.isInverseSurrogateFoldOf(test);
		w.isSurrogateFoldOf(test);
		w.isSurrogateNeighborOf(test);

		assertEquals(new WebId(1).isSurrogateFoldOf(test), true);

		assertEquals(w.isSurrogateNeighborOf(new WebId(2)), false);

		test.toString();

		assertEquals(test.hashCode(), new WebId(0).hashCode());

		test.setHeight(0);
		test.toString();
		test.getParentsValue();

		WebId.getLowestBitPos(5);
		WebId.getLowestBitPos(10);
		WebId.getLowestBitPos(100);

		test.isFoldOf(test);
		test.isInverseSurrogateFoldOf(test);
		test.isNeighborOf(test);
		test.isSurrogateFoldOf(test);
		test.isSurrogateNeighborOf(test);

		WebId n = new WebId(1, 1);
		WebId m = new WebId(2, 2);
		n.isFoldOf(m);
		n.isInverseSurrogateFoldOf(m);
		n.isNeighborOf(m);
		n.isSurrogateFoldOf(m);
		n.isSurrogateNeighborOf(m);

		m.isFoldOf(n);
		m.isInverseSurrogateFoldOf(n);
		m.isNeighborOf(n);
		m.isSurrogateFoldOf(n);
		m.isSurrogateNeighborOf(n);

		test.isInverseSurrogateFoldOf(test);
		test.isNeighborOf(test);
		test.isSurrogateFoldOf(test);
		test.isSurrogateNeighborOf(test);

		// Invalid

		WebId test2 = new WebId(-1);
		test2.toString();
		test2 = new WebId(-1, -1);
		test2.toString();
		test2 = new WebId(1, -1);
		test2 = new WebId(-1, 1);
		test2 = new WebId(1, 35);

		test.equals(null);
		test.isNeighborOf(null);
		test.isSurrogateFoldOf(null);
		test.isSurrogateNeighborOf(null);
		test.isFoldOf(null);

		test.equals(WebId.NULL_WEB_ID);
		test.equals(HyPeerWeb.getSingleton());
		test.isNeighborOf(WebId.NULL_WEB_ID);
		test.isSurrogateFoldOf(WebId.NULL_WEB_ID);
		test.isSurrogateNeighborOf(WebId.NULL_WEB_ID);
		test.isFoldOf(WebId.NULL_WEB_ID);

		test2.equals(null);
		test2.isNeighborOf(null);
		test2.isSurrogateFoldOf(null);
		test2.isSurrogateNeighborOf(null);

		test2.equals(WebId.NULL_WEB_ID);
		test2.equals(HyPeerWeb.getSingleton());
		test2.isNeighborOf(WebId.NULL_WEB_ID);
		test2.isSurrogateFoldOf(WebId.NULL_WEB_ID);
		test2.isSurrogateNeighborOf(WebId.NULL_WEB_ID);

		test.setHeight(-1);
		test.toString();

		WebId.getLowestBitPos(0);
		WebId.getLowestBitPos(-1);

		test.distanceTo(new WebId(-1));
		test2.distanceTo(test);

		WebId wd = WebId.NULL_WEB_ID;
		wd.equals(WebId.NULL_WEB_ID);
		wd.equals(HyPeerWeb.getSingleton());
		wd.isNeighborOf(WebId.NULL_WEB_ID);
		wd.isSurrogateFoldOf(WebId.NULL_WEB_ID);
		wd.isSurrogateNeighborOf(WebId.NULL_WEB_ID);
		wd.isInverseSurrogateFoldOf(WebId.NULL_WEB_ID);
		wd.isFoldOf(wd);
		wd.isInverseSurrogateFoldOf(null);
		wd.distanceTo(wd);
		WebId.locationOfMostSignificantOneBit(-1);
		wd.toString();
		wd.getParentsValue();
		test = new WebId(10, 4);
		test.getParentsValue();
		WebId.getLowestBitPos(1);
		test.incrementHeight();

	}
}