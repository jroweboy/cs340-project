package phase4.classes;

import java.io.Serializable;

import dbPhase.hypeerweb.Node;

/**
 * A visitor class that sends a message from one node in a hypeerweb to another.<br>
 * <br>
 * 
 * <pre>
 * <b>Domain:</b>
 * 		None
 * 
 * <b>Invariant:</b>
 * 		None
 * 
 * </pre>
 * 
 * @author Thomas Holladay
 * @author Robert Hickman
 */
public abstract class SendVisitor implements Visitor, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4245096786551984227L;

	/**
	 * The base constructor for the class.
	 * 
	 * @author Robert Hickman
	 */
	public SendVisitor() {
	}

	/**
	 * Constructs and returns the parameters for the visit method with at least
	 * a target.
	 * 
	 * @author Thomas Holladay
	 * @param target
	 *            the webId as an integer of the node to send to
	 * @pre target &ge; 0
	 * @post result is a Parameters object with one of its key value pairs being
	 *       "target" => target.
	 */
	public static Parameters createInitialParameters(int target) {
		assert target >= 0;

		Parameters p = new Parameters();
		p.set("target", target);
		p.set("testing", "testing");
		return p;
	}

	/**
	 * Uses the visitor pattern to visit each node until it reaches the target
	 * specified in the parameters.
	 * 
	 * @author Robert Hickman
	 * @param node
	 * @param parameters
	 * @pre parameters.get("target") != null AND parameters.get("target") %gt; 0
	 *      AND parameters.get("target") %le
	 *      node.findZero().findBiggest().getValue();
	 * @post If the current node is the target node, then call targetOperation,
	 *       otherwise call intermediateOperation and visit a random node from
	 *       the set of nodes closest to the target node. Do not alter the
	 *       parameters.
	 */
	@Override
	public void visit(Node node, Parameters parameters) {
		// TODO check for type error?
		if (parameters.get("target") == null
				|| (int) parameters.get("target") < 0) {
			return;
		}

		int destinationId = (int) parameters.get("target");
		if (destinationId > node.findZero().findBiggest().getValue()) {
			System.out.println("The target node with webId " + node.getValue()
					+ " is not in the hypeerweb.");
		} else if (node.getValue() == destinationId) {
			targetOperation(node, parameters);
		} else {
			Node closest = node;
			int difference = Integer.bitCount(closest.getValue()
					^ destinationId);
			for (Node n : node.getAllConnections()) {
				int currentDifference = Integer.bitCount(n.getValue()
						^ destinationId);
				if (currentDifference < difference
						|| (currentDifference == difference && Math.floor(Math
								.random() * 2) == 1)) {
					closest = n;
					difference = Integer.bitCount(closest.getValue()
							^ destinationId);
				}
			}
			intermediateOperation(node, parameters);
			visit(closest, parameters);
		}
	}

	/**
	 * The method that is called when the visitor reaches the target node.
	 * 
	 * @author Thomas Holladay
	 * @param node
	 * @param parameters
	 * @pre None
	 * @post None
	 */
	protected abstract void targetOperation(Node node, Parameters parameters);

	/**
	 * The method that is called each time the visitor goes to a new node except
	 * the last one.
	 * 
	 * @author Robert Hickman
	 * @param node
	 * @param parameters
	 * @pre None
	 * @post None
	 */
	protected void intermediateOperation(Node node, Parameters parameters) {

	}

}
