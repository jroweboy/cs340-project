package phase3.classes;

import dbPhase.hypeerweb.Node;

public class InverseSurrogateNeighbor extends Node {

	Node me;

	public InverseSurrogateNeighbor(Node n) {
		super(n);
		me = n;
	}

	@Override
	public void connectTo(Node n, Node parent) {
		me.removeDownPointer(parent);
		parent.removeUpPointer(me);

		n.addNeighbor(me);
		me.addNeighbor(n);

	}

	@Override
	public void disconnectFrom(Node n, Node parent) {
		System.out.println("Shouldn't get called");
	}

	@Override
	public void replace(Node oldNode, Node newNode) {
		me.removeDownPointer(oldNode);
		me.addDownPointer(newNode);

		oldNode.removeUpPointer(me);
		newNode.addUpPointer(me);
		me.changeStatesOfNodes();
		newNode.changeStatesOfNodes();
	}

}
