package phase3.classes;

import dbPhase.hypeerweb.Node;

public class SurrogateFold extends Node {
	protected Node me;

	// HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
	public SurrogateFold(Node n) {
		super(n);
		me = n;
	}

	@Override
	public void connectTo(Node n, Node parent) {
		me.setSurrogateFold(n);
		n.setSurrogateFold(me);
	}

	@Override
	public void disconnectFrom(Node n, Node parent) {
		me.setSurrogateFold(Node.NULL_NODE);
		n.setSurrogateFold(Node.NULL_NODE);
		// database.updateFold(me);
		// database.updateFold(n);
	}

	@Override
	public void replace(Node oldNode, Node newNode) {
		me.setInverseSurrogateFold(newNode);
		oldNode.setSurrogateFold(Node.NULL_NODE);
		newNode.setSurrogateFold(me);
		// database.updateFold(me);
		// database.updateFold(oldNode);
		// database.updateFold(newNode);
	}

}
