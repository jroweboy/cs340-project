package phase3.classes;

import dbPhase.hypeerweb.Node;

public class Neighbor extends Node {

	Node me;

	// HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();

	public Neighbor(Node n) {
		super(n);
		me = n;
	}

	@Override
	public void connectTo(Node n, Node parent) {
		if (me.getValue() > parent.getValue()) {
			n.addDownPointer(me);
			me.addUpPointer(n);
		}
	}

	@Override
	public void disconnectFrom(Node n, Node parent) {
		// System.out.println(me+"  "+parent);
		if (me == parent) {
			// System.out.println("Got Here");
			if (me.getSFold() != Node.NULL_NODE) {
				me.setFold(me.getSFold());
				// database.updateFold(me);
			} else {
				// me.setInverseSurrogateFold(n.getFold());
			}
		} else {
			// System.out.println("Got 2 HERE");
			parent.addUpPointer(me);
			me.addDownPointer(parent);
			// database.addNeighborQueue(me, parent, "sn");
		}
		me.removeNeighbor(n);
		n.removeNeighbor(me);
		// database.removeNeighborQueue(me, n);
		// database.removeNeighborQueue(n, me);
	}

	@Override
	public void replace(Node oldNode, Node newNode) {
		me.removeNeighbor(oldNode);
		// database.removeNeighborQueue(me, oldNode);
		me.addNeighbor(newNode);
		// database.addNeighborQueue(me, newNode, "n");

		oldNode.removeNeighbor(me);
		// database.removeNeighborQueue(oldNode, me);
		newNode.addNeighbor(me);
		// database.addNeighborQueue(newNode, me, "n");

		// newNode.changeStatesOfNodes();
	}

}
