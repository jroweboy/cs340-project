package phase3.classes;

import junit.framework.TestCase;
import phase2.states.StandardNodeState;
import dbPhase.hypeerweb.HyPeerWeb;
import dbPhase.hypeerweb.Node;

public class RemovingNodes extends TestCase {

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	public void testRemoveOneFromSizeTwo() {
		HyPeerWeb h = getHypeerWebSize(2);
		Node n = h.getNode(0);
		h.removeNode(n);

		// TEST TO SEE IF SIZE IS CORRECT
		assertEquals(h.size(), 1);
		// TEST ALL NODES IF THEY ARE CORRECT
		Node n0 = h.getNode(0);

		// NODE 0
		assertEquals(n0.getValue(), 0);
		assertEquals(n0.getFold(), n0);
		assertEquals(n0.getSFold(), Node.NULL_NODE);
		assertEquals(n0.getISFold(), Node.NULL_NODE);
		assertEquals(n0.getNeighbors().size(), 0);
		assertEquals(n0.getDownPointers().size(), 0);
		assertEquals(n0.getUpPointers().size(), 0);
		assertEquals(n0.getState().getNodeState(),
				new StandardNodeState().getNodeState());

	}

	// public void testRemoveOneFromSizeSeven() {
	// HyPeerWeb h = getHypeerWebSize(7);
	// Node n = h.getNode(2);
	// h.removeNode(n);
	//
	// // TEST TO SEE IF SIZE IS CORRECT
	// assertEquals(h.size(), 6);
	// //TEST ALL NODES IF THEY ARE CORRECT
	// Node n0 = h.getNode(0);
	// Node n1 = h.getNode(1);
	// Node n2 = h.getNode(2);
	// Node n3 = h.getNode(3);
	// Node n4 = h.getNode(4);
	// Node n5 = h.getNode(5);
	//
	//
	//
	// //NODE 0
	// assertEquals(n0.getValue(), 0);
	// assertEquals(n0.getFold(), Node.NULL_NODE);
	// assertEquals(n0.getSFold(), n3);
	// assertEquals(n0.getISFold(), Node.NULL_NODE);
	// assertEquals(n0.getNeighbors().size(),3);
	// assertEquals(n0.getDownPointers().size(),0);
	// assertEquals(n0.getUpPointers().size(),0);
	// assertEquals(n0.getNeighbors().get(0),n1);
	// assertEquals(n0.getNeighbors().get(1),n2);
	// assertEquals(n0.getNeighbors().get(1),n4);
	// assertEquals(n0.getState().getNodeState(),new
	// StandardNodeState().getNodeState());
	//
	// //NODE 1
	// assertEquals(n1.getValue(), 1);
	// assertEquals(n1.getFold(),Node.NULL_NODE);
	// assertEquals(n1.getSFold(), n2);
	// assertEquals(n1.getISFold(), Node.NULL_NODE);
	// assertEquals(n1.getNeighbors().size(),3);
	// assertEquals(n1.getDownPointers().size(),0);
	// assertEquals(n1.getUpPointers().size(),0);
	// assertEquals(n1.getNeighbors().get(0),n0);
	// assertEquals(n1.getNeighbors().get(1),n3);
	// assertEquals(n1.getNeighbors().get(1),n5);
	// assertEquals(n1.getState().getNodeState(),new
	// StandardNodeState().getNodeState());
	//
	// //NODE 2
	// assertEquals(n2.getValue(), 2);
	// assertEquals(n2.getFold(),n5);
	// assertEquals(n2.getSFold(), Node.NULL_NODE);
	// assertEquals(n2.getISFold(), n1);
	// assertEquals(n2.getNeighbors().size(),2);
	// assertEquals(n2.getDownPointers().size(),0);
	// assertEquals(n2.getUpPointers().size(),1);
	// assertEquals(n2.getNeighbors().get(0),n0);
	// assertEquals(n2.getNeighbors().get(1),n3);
	// assertEquals(n2.getState().getNodeState(),new
	// DownPointingNodeState().getNodeState());
	//
	// //NODE 3
	// assertEquals(n3.getValue(), 3);
	// assertEquals(n3.getFold(),n4);
	// assertEquals(n3.getSFold(), Node.NULL_NODE);
	// assertEquals(n3.getISFold(), n0);
	// assertEquals(n3.getNeighbors().size(),2);
	// assertEquals(n3.getDownPointers().size(),0);
	// assertEquals(n3.getUpPointers().size(),1);
	// assertEquals(n3.getNeighbors().get(0),n1);
	// assertEquals(n3.getNeighbors().get(1),n2);
	// assertEquals(n3.getState().getNodeState(),new
	// DownPointingNodeState().getNodeState());
	//
	// //NODE 4
	// assertEquals(n4.getValue(), 4);
	// assertEquals(n4.getFold(),n3);
	// assertEquals(n4.getSFold(), Node.NULL_NODE);
	// assertEquals(n4.getISFold(), Node.NULL_NODE);
	// assertEquals(n4.getNeighbors().size(),2);
	// assertEquals(n4.getDownPointers().size(),1);
	// assertEquals(n4.getUpPointers().size(),0);
	// assertEquals(n4.getNeighbors().get(0),n0);
	// assertEquals(n4.getNeighbors().get(1),n5);
	// assertEquals(n4.getState().getNodeState(),new
	// UpPointingNodeState().getNodeState());
	//
	// //NODE 5
	// assertEquals(n5.getValue(), 5);
	// assertEquals(n5.getFold(),n2);
	// assertEquals(n5.getSFold(), Node.NULL_NODE);
	// assertEquals(n5.getISFold(), Node.NULL_NODE);
	// assertEquals(n5.getNeighbors().size(),2);
	// assertEquals(n5.getDownPointers().size(),1);
	// assertEquals(n5.getUpPointers().size(),0);
	// assertEquals(n5.getNeighbors().get(0),n4);
	// assertEquals(n5.getNeighbors().get(1),n1);
	// assertEquals(n5.getState().getNodeState(),new
	// UpPointingNodeState().getNodeState());
	//
	//
	// h.clear();
	// }

	private HyPeerWeb getHypeerWebSize(int size) {
		// TODO Auto-generated method stub
		HyPeerWeb h = HyPeerWeb.getSingleton();
		for (int i = 0; i < size; i++) {
			Node node = new Node(i);
			h.addToHyPeerWeb(node);
		}
		return h;
	}

}
