package phase3.classes;

import dbPhase.hypeerweb.Node;

public class Fold extends Node {
	private Node me;

	// HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();

	public Fold(Node n) {
		super(n);
		me = n;
	}

	@Override
	public void connectTo(Node n, Node parent) {

		if (parent.getISFold() != Node.NULL_NODE) {
			n.setFold(parent.getISFold());
			n.getFold().setFold(n);
			n.getFold().setSurrogateFold(Node.NULL_NODE);
			parent.setInverseSurrogateFold(Node.NULL_NODE);
		} else {
			n.setFold(me);
			me.getFold().setSurrogateFold(me);
			me.getFold().setFold(Node.NULL_NODE);
			me.setInverseSurrogateFold(me.getFold());
			me.setFold(n);
		}

	}

	@Override
	public void disconnectFrom(Node n, Node parent) {

		if (!me.getISFold().equals(Node.NULL_NODE)) {
			me.setFold(me.getISFold());
			me.getISFold().setSurrogateFold(Node.NULL_NODE);
			// database.updateFold(me.getISFold());
			me.setInverseSurrogateFold(Node.NULL_NODE);
			me.getFold().setFold(me);
			// database.updateFold(me.getFold());
		} else {
			if (!me.equals(parent)) {
				me.setSurrogateFold(parent);
				me.getSFold().setInverseSurrogateFold(me);
				me.getSFold().setSurrogateFold(Node.NULL_NODE);
				// database.updateFold(me.getISFold());
				me.setFold(Node.NULL_NODE);
			} else {
				me.setFold(me);
				me.setSurrogateFold(Node.NULL_NODE);
			}
		}
		// database.updateFold(me);

		n.setFold(Node.NULL_NODE);
	}

	@Override
	public void replace(Node oldNode, Node newNode) {
		// System.out.println("Fold replace HAS BEEN CALLED WOOO");
		me.setFold(newNode);
		oldNode.setFold(Node.NULL_NODE);
		newNode.setFold(me);
		// database.updateFold(me);
		// database.updateFold(oldNode);
		// database.updateFold(newNode);
	}
}
