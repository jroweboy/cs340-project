package phase3.classes;

import dbPhase.hypeerweb.Node;

public class SurrogateNeighbor extends Node {

	protected Node me;

	// HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();

	public SurrogateNeighbor(Node n) {
		super(n);
		me = n;
	}

	public void connectTo(Node n) {
		me.addUpPointer(n);
		n.addDownPointer(me);
	}

	@Override
	public void disconnectFrom(Node n, Node parent) {
		me.removeUpPointer(n);
		n.removeDownPointer(me);
		me.changeStatesOfNodes();
		// database.removeNeighborQueue(n, me);
		// database.updateFold(me);
	}

	@Override
	public void replace(Node oldNode, Node newNode) {
		me.removeUpPointer(oldNode);
		me.addUpPointer(newNode);

		oldNode.removeDownPointer(me);
		newNode.addDownPointer(me);
		// database.addNeighborQueue(newNode, me, "sn");
		// database.removeNeighborQueue(oldNode, me);

		me.changeStatesOfNodes();
		newNode.changeStatesOfNodes();
		// database.updateFold(me);
		// database.updateFold(newNode);
	}

}
