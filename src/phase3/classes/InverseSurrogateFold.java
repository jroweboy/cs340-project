package phase3.classes;

import dbPhase.hypeerweb.Node;

public class InverseSurrogateFold extends Node {

	Node me;

	public InverseSurrogateFold(Node n) {
		super(n);
		me = n;
	}

	@Override
	public void connectTo(Node n, Node parent) {
		me.setSurrogateFold(n);
		n.setInverseSurrogateFold(me);
	}

	@Override
	public void disconnectFrom(Node n, Node parent) {
		System.out.println("Shouldn't get called");
	}

	@Override
	public void replace(Node oldNode, Node newNode) {
		me.setSurrogateFold(newNode);
		oldNode.setInverseSurrogateFold(Node.NULL_NODE);
		newNode.setInverseSurrogateFold(me);
	}

}
