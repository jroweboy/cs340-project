package dbPhase.hypeerweb;

import java.io.ObjectStreamException;
import java.util.ArrayList;

import phase2.states.NodeState;
import phase4.classes.Contents;
import phase4.classes.Parameters;
import phase4.classes.Visitor;
import phase6.Command;
import phase6.GlobalObjectId;
import phase6.LocalObjectId;
import phase6.MachineAddress;
import phase6.ObjectDB;
import phase6.PeerCommunicator;

public class NodeProxy extends Node {
	private GlobalObjectId globalObjectId;

	public NodeProxy(GlobalObjectId globalObjectId) {
		super(0);
		this.globalObjectId = globalObjectId;
	}

	@Override
	public GlobalObjectId getGlobalId() {
		return globalObjectId;
	}

	@Override
	public boolean equals(java.lang.Object p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "java.lang.Object";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "equals", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Boolean) result;
	}

	@Override
	public java.lang.String toString() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "toString", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (java.lang.String) result;
	}

	@Override
	public int compareTo(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "compareTo", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Integer) result;
	}

	/*
	 * public volatile int compareTo(Object p0) { String[] parameterTypeNames =
	 * new String[1]; parameterTypeNames[0] = "java.lang.Object"; Object[]
	 * actualParameters = new Object[1]; actualParameters[0] = p0; Command
	 * command = new Command(globalObjectId.getLocalObjectId(), "Node",
	 * "compareTo", parameterTypeNames, actualParameters, true); Object result =
	 * PeerCommunicator.getSingleton().sendSynchronous( globalObjectId,
	 * command); return (Integer) result; }
	 */

	@Override
	public void replace(Node p0, Node p1) {
		String[] parameterTypeNames = new String[2];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		parameterTypeNames[1] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[2];
		actualParameters[0] = p0;
		actualParameters[1] = p1;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "replace", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public int getValue() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getValue", parameterTypeNames,
				actualParameters, true);
		if (PeerCommunicator.getSingleton() == null) {
			System.err.println("Proxy doesn't exist");
			return 99999;
		}
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Integer) result;
	}

	@Override
	public Node find(WebId p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.WebId";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "find", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Node) result;
	}

	@Override
	public NodeState getState() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getState", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (NodeState) result;
	}

	@Override
	public void accept(Visitor p0, Parameters p1) {
		String[] parameterTypeNames = new String[2];
		parameterTypeNames[0] = "phase4.classes.Visitor";
		parameterTypeNames[1] = "phase4.classes.Parameters";
		Object[] actualParameters = new Object[2];
		actualParameters[0] = p0;
		actualParameters[1] = p1;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "accept", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void setState(NodeState p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "phase2.states.NodeState";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "setState", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public WebId getWebId() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getWebId", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (WebId) result;
	}

	@Override
	public Node getFold() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getFold", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Node) result;
	}

	@Override
	public Node getSFold() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getSFold", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Node) result;
	}

	@Override
	public Node getISFold() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getISFold", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Node) result;
	}

	@Override
	@SuppressWarnings("unchecked")
	public ArrayList<Node> getNeighbors() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getNeighbors", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (ArrayList<Node>) result;
	}

	@Override
	public void addDownPointer(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "addDownPointer", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void addNeighbor(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "addNeighbor", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void addUpPointer(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "addUpPointer", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public SimplifiedNodeDomain constructSimplifiedNodeDomain() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "constructSimplifiedNodeDomain",
				parameterTypeNames, actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (SimplifiedNodeDomain) result;
	}

	@Override
	public void removeDownPointer(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "removeDownPointer",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void removeNeighbor(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "removeNeighbor", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void removeUpPointer(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "removeUpPointer",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void setFold(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "setFold", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void setInverseSurrogateFold(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "setInverseSurrogateFold",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void setSurrogateFold(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "setSurrogateFold",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void setWebId(WebId p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.WebId";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "setWebId", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	@SuppressWarnings("unchecked")
	public ArrayList<Node> getUpPointers() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getUpPointers", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (ArrayList<Node>) result;
	}

	@Override
	@SuppressWarnings("unchecked")
	public ArrayList<Node> getDownPointers() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getDownPointers",
				parameterTypeNames, actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (ArrayList<Node>) result;
	}

	@Override
	@SuppressWarnings("unchecked")
	public ArrayList<Node> getAllConnections() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getAllConnections",
				parameterTypeNames, actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (ArrayList<Node>) result;
	}

	@Override
	public void addToHyPeerWeb(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "addToHyPeerWeb", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public Node findBiggest() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "findBiggest", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Node) result;
	}

	@Override
	public void changeStatesOfNodes() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "changeStatesOfNodes",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public int removeFromHyPeerWeb(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "removeFromHyPeerWeb",
				parameterTypeNames, actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Integer) result;
	}

	@Override
	public void replaceMe(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "replaceMe", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void initializeChild(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "initializeChild",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void connectTo(Node p0, Node p1) {
		String[] parameterTypeNames = new String[2];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		parameterTypeNames[1] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[2];
		actualParameters[0] = p0;
		actualParameters[1] = p1;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "connectTo", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void disconnectFrom(Node p0, Node p1) {
		String[] parameterTypeNames = new String[2];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		parameterTypeNames[1] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[2];
		actualParameters[0] = p0;
		actualParameters[1] = p1;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "disconnectFrom", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public Node findParent() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "findParent", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Node) result;
	}

	@Override
	public boolean isHypercubeCapNode(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "isHypercubeCapNode",
				parameterTypeNames, actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Boolean) result;
	}

	@Override
	public java.lang.String ourToString() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "ourToString", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (java.lang.String) result;
	}

	@Override
	public void clearConnections() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "clearConnections",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	public Contents getMyContents() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getMyContents", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Contents) result;
	}

	@Override
	public void setMyContents(Contents p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "Contents";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "setMyContents", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public Contents getContents() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getContents", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Contents) result;
	}

	@Override
	public int hashCode() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"java.lang.Object", "hashCode", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Integer) result;
	}

	private Object writeReplace() throws ObjectStreamException {
		System.out.println("NodeProxy writereplace");
		return this;
	}

	private Object readResolve() throws ObjectStreamException {
		System.out.println("NodeProxy readresolve");
		GlobalObjectId possiblyOtherId = new GlobalObjectId(MachineAddress
				.getThisMachinesInetAddress().getHostAddress(), HyPeerWeb.port,
				new LocalObjectId());
		if (possiblyOtherId.onSameMachineAs(this.globalObjectId)) {
			return ObjectDB.getSingleton().getValue(
					this.globalObjectId.getLocalObjectId());
		} else {
			return this;
		}
	}

	@Override
	public LocalObjectId getLocalObjectId() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getLocalObjectId",
				parameterTypeNames, actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		try {
			((Exception) result).printStackTrace();
		} catch (Exception e) {
		}
		return (LocalObjectId) result;
	}

}
