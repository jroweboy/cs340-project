package dbPhase.hypeerweb;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import phase2.states.CreateConnections;
import phase2.states.DownPointingNodeState;
import phase2.states.HypercubeCapState;
import phase2.states.NodeState;
import phase2.states.StandardNodeState;
import phase2.states.TerminalNodeState;
import phase2.states.UpPointingNodeState;
import phase3.classes.Fold;
import phase3.classes.InverseSurrogateFold;
import phase3.classes.InverseSurrogateNeighbor;
import phase3.classes.Neighbor;
import phase3.classes.SurrogateFold;
import phase3.classes.SurrogateNeighbor;
import phase4.classes.Contents;
import phase4.classes.Parameters;
import phase4.classes.Visitor;
import phase6.GlobalObjectId;
import phase6.LocalObjectId;

/**
 * A node in the HyPeerWeb.
 * 
 * @author jrowe7
 * 
 */
public class Node implements Serializable, Comparable<Node> { //

	/**
	 * 
	 */
	private static final long serialVersionUID = 3625672156215926687L;

	public static final Node NULL_NODE = new Node(-1) {
		@Override
		public void addToHyPeerWeb(Node node) {

		}

		@Override
		public void addDownPointer(Node downPointer) {
		}

		@Override
		public void addNeighbor(Node neighbor) {
		}

		@Override
		public void addUpPointer(Node upPointer) {
		}

		@Override
		public SimplifiedNodeDomain constructSimplifiedNodeDomain() {
			return null;
		}

		@Override
		public WebId getWebId() {
			return WebId.NULL_WEB_ID;
		}

		@Override
		public void removeDownPointer(Node downPointer) {
		}

		@Override
		public void removeNeighbor(Node neighbor) {
		}

		@Override
		public void removeUpPointer(Node upPointer) {
		}

		@Override
		public void setFold(Node newFold) {
		}

		@Override
		public void setInverseSurrogateFold(Node newInverseSurrogateFold) {
		}

		@Override
		public void setSurrogateFold(Node newSurrogateFold) {
		}

		@Override
		public void setWebId(WebId webId) {
		}

		@Override
		public void replace(Node node, Node target) {

		}

		@Override
		public boolean equals(Object node) {
			return ((Node) node).getValue() == Node.NULL_NODE.getValue();
		}

		@Override
		public int getValue() {
			return -1;
		}
	};

	protected Height height;
	protected WebId webId;

	protected Node fold;
	protected Node sFold;
	protected Node isFold;
	protected ArrayList<Node> upPointers = new ArrayList<Node>();
	protected ArrayList<Node> downPointers = new ArrayList<Node>();
	protected ArrayList<Node> neighbors = new ArrayList<Node>();
	protected NodeState state = new StandardNodeState();
	protected Contents myContents = null;
	private GlobalObjectId globalObjectId;

	/**
	 * The constructor for a node given only the new id.
	 * 
	 * @param id
	 */
	public Node(int id) {
		this.webId = new WebId(id);
		this.height = new Height(0);
		baseConstructor();
	}

	/**
	 * The constructor for a node given the new id and height.
	 * 
	 * @param id
	 * @param height
	 */
	public Node(int id, int height) {
		this.webId = new WebId(id, height);
		this.height = new Height(height);
		baseConstructor();
	}

	protected Node(Node n) {
		this.height = new Height(n.getHeight());
		this.webId = n.getWebId();

		this.fold = n.getFold();
		this.sFold = n.getSFold();
		this.isFold = n.getISFold();
		this.upPointers = n.upPointers;
		this.downPointers = n.downPointers;
		this.neighbors = n.getNeighbors();
		this.state = n.getState();
	}

	private void baseConstructor() {
		this.fold = this;
		this.isFold = NULL_NODE;
		this.sFold = NULL_NODE;

		// TODO Set global object ID;
		try {
			globalObjectId = new GlobalObjectId(InetAddress.getLocalHost()
					.getHostAddress(), HyPeerWeb.port, new LocalObjectId());
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public GlobalObjectId getGlobalId() {
		return globalObjectId;
	}

	/**
	 * The constructor for a node given the new id and height.
	 * 
	 * @param id
	 * @param height
	 */
	public Node(WebId id) {
		this.webId = id;
		this.height = new Height(0);
	}

	public NodeState getState() {
		return state;
	}

	public void setState(NodeState s) {
		state = s;
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		if (database.isDynamic()) {
			database.update(this);
		}
	}

	/**
	 * 
	 * @param downPointer
	 */
	public void addDownPointer(Node downPointer) {
		height.inc();
		downPointers.add(downPointer);
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		if (database.isDynamic()) {
			database.addNeighborQueue(this, downPointer, "sn");
			database.update(this);
		}
	}

	/**
	 * 
	 * @param neighbor
	 */
	public void addNeighbor(Node neighbor) {
		height.inc();
		neighbors.add(neighbor);
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		if (database.isDynamic()) {
			database.addNeighborQueue(this, neighbor, "n");
			database.update(this);
		}
	}

	/**
	 * 
	 * @param upPointer
	 */
	public void addUpPointer(Node upPointer) {
		upPointers.add(upPointer);
	}

	/**
	 * 
	 * @return
	 */
	public SimplifiedNodeDomain constructSimplifiedNodeDomain() {
		HashSet<Integer> neigh = new HashSet<Integer>();
		HashSet<Integer> up = new HashSet<Integer>();
		HashSet<Integer> down = new HashSet<Integer>();
		for (Node n : neighbors) {
			neigh.add(n.getValue());
		}
		for (Node n : upPointers) {
			up.add(n.getValue());
		}
		for (Node n : downPointers) {
			down.add(n.getValue());
		}

		return new SimplifiedNodeDomain(webId.getValue(), getHeight(), neigh,
				up, down, fold.getValue(), sFold.getValue(), isFold.getValue(),
				state.getNodeState());
	}

	/**
	 * 
	 * @return
	 */
	public WebId getWebId() {
		return webId;
	}

	/**
	 * 
	 * @param downPointer
	 */
	public void removeDownPointer(Node downPointer) {
		for (Node d : downPointers) {
			if (d.equals(downPointer)) {
				height.dec();
				downPointers.remove(d);
				HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
				if (database.isDynamic()) {
					database.removeNeighborQueue(this, downPointer);
					database.update(this);
				}
				break;
			}
		}
	}

	/**
	 * 
	 * @param neighbor
	 */
	public void removeNeighbor(Node neighbor) {
		for (Node n : neighbors) {
			if (n.equals(neighbor)) {
				height.dec();
				neighbors.remove(n);
				HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
				if (database.isDynamic()) {
					database.removeNeighborQueue(this, neighbor);
					database.update(this);
				}
				return;
			}
		}
	}

	/**
	 * 
	 * @param upPointer
	 */
	public void removeUpPointer(Node upPointer) {
		for (Node u : upPointers) {
			if (u.equals(upPointer)) {
				upPointers.remove(u);
				return;
			}
		}

	}

	/**
	 * 
	 * @param newFold
	 */
	public void setFold(Node newFold) {
		fold = newFold;
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		if (database.isDynamic()) {
			database.update(this);
		}
	}

	/**
	 * 
	 * @param newInverseSurrogateFold
	 */
	public void setInverseSurrogateFold(Node newInverseSurrogateFold) {
		isFold = newInverseSurrogateFold;
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		if (database.isDynamic()) {
			database.update(this);
		}
	}

	/**
	 * 
	 * @param newSurrogateFold
	 */
	public void setSurrogateFold(Node newSurrogateFold) {
		sFold = newSurrogateFold;
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		if (database.isDynamic()) {
			database.update(this);
		}
	}

	/**
	 * 
	 * @param webId
	 */
	public void setWebId(WebId webId) {
		this.webId = webId;
	}

	public void setDynamic(boolean isDynamic) {
		height.setDynamic(isDynamic);
	}

	/**
	 * 
	 * @Override
	 */
	@Override
	public boolean equals(Object o) {
		return ((Node) o).getValue() == this.getValue();
	}

	public Node getFold() {
		return fold;
	}

	public Node getSFold() {
		return sFold;
	}

	public Node getISFold() {
		return isFold;
	}

	public ArrayList<Node> getNeighbors() {
		return neighbors;
	}

	public ArrayList<Node> getUpPointers() {
		return upPointers;
	}

	public ArrayList<Node> getDownPointers() {
		return downPointers;
	}

	public Node findZero() {
		if (this.getValue() == 0 || this.getValue() == -1) {
			return this;
		}
		Node lowest = NULL_NODE;
		int thisId = this.getValue();

		ArrayList<Node> allConnections = getAllConnections();
		if (allConnections.size() == 0) {
			return this;
		}
		// allConnections.addAll(neighbors);
		// allConnections.addAll(downPointers);
		// allConnections.addAll(upPointers);
		// allConnections.add(fold);
		// allConnections.add(sFold);
		// allConnections.add(isFold);
		for (Node n : allConnections) {
			int testId = n.getValue();
			if (0 <= testId && testId < thisId) {
				if (lowest.equals(NULL_NODE) || testId < lowest.getValue()) {
					lowest = n;
				}
			}
		}
		return lowest.findZero();
	}

	/**
	 * @author Thomas Holladay
	 * @param node
	 * @return
	 */
	public void addToHyPeerWeb(Node node) {
		int newWebId;
		if (webId.getValue() > 0) {
			newWebId = findZero().findBiggest().getWebId().getValue() + 1;
		} else {
			newWebId = findBiggest().getWebId().getValue() + 1;
		}
		node.setWebId(new WebId(newWebId));
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.addQueue(node);
		CreateConnections conn = new CreateConnections();
		Node insertionPoint = findInsertionPoint().lower;
		conn.makeConnections(insertionPoint, node);
		insertionPoint.changeStatesOfNodes();
		node.changeStatesOfNodes();
		for (Node n : node.getAllConnections()) {
			n.changeStatesOfNodes();
		}
		database.commit();
	}

	/**
	 * @author Thomas Holladay
	 * @param node
	 * @return
	 */
	public int removeFromHyPeerWeb(Node node) {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		Node terminalNode = findBiggest();
		LowerUpperBound bounds = terminalNode.findInsertionPoint();
		ArrayList<Node> allNodes = node.getAllConnections();
		// James: Add all will add ALL connections including duplicates AND the
		// node we just removed. Not a good idea
		// allNodes.addAll(bounds.upper.getAllConnections());
		for (Node n : bounds.upper.getAllConnections()) {
			if (!allNodes.contains(n) && !n.equals(node)) {
				allNodes.add(n);
			}
		}
		int replacement = bounds.upper.getValue();
		bounds.upper.replaceMe(node);
		database.deleteReplace(node, replacement);
		for (Node n : allNodes) {
			n.changeStatesOfNodes();
		}
		bounds.upper.changeStatesOfNodes();
		database.commit();
		return node.getValue();
	}

	public void initializeChild(Node c) {
		// TODO change the connections
		// this.connections = connections;
		final Node parent = this;
		final Node child = c;
		// System.out.println("Adding Node "+child+ " to "+parent);
		new OperateOnConnections(child) { // AddConnections

			@Override
			public Iterator<Node> getIterator() {
				ArrayList<Node> nodes = new ArrayList<Node>();

				nodes.add(new Fold(fold));

				for (Node n : neighbors) {
					nodes.add(new Neighbor(n));
				}
				// nodes.addAll(neighbors);
				for (Node n : upPointers) {
					nodes.add(new InverseSurrogateNeighbor(n));
				}
				return nodes.iterator();
			}

			@Override
			public void modify(Node node, Node target) {
				node.connectTo(target, parent);
			}
		}.execute();

	}

	// protected void addNewConnection(Node target) {
	// TODO Auto-generated method stub
	// probably don't need to use these if we use the Fold classes and stuff
	// }

	// protected void removeConnection(Node target) {

	// }

	public void connectTo(Node node, Node parent) {
		System.out.println("ConnectTo called on the wrong FRIGGIN CLASS NOOB");
	}

	public void disconnectFrom(Node node, Node parent) {
		System.out.println("Disconnect called on the wrong FRIGGIN CLASS NOOB");
	}

	public void replace(Node old, Node n) {
		System.out.println("Replace called on the wrong FRIGGIN CLASS NOOB");
	}

	/**
	 * @author Thomas Holladay
	 * @param node
	 * @return
	 */
	public void replaceMe(Node leavingNode) {
		// CreateConnections conn = new CreateConnections();
		final Node replacingNode = this;
		final Node parent = this.findParent();
		// HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		new OperateOnConnections(this) {

			@Override
			public void modify(Node node, Node target) {
				node.disconnectFrom(target, parent);
			}
		}.execute();

		new OperateOnConnections(leavingNode) {
			@Override
			public Iterator<Node> getIterator() {
				ArrayList<Node> nodes = new ArrayList<Node>();

				// for (Node n :) {
				// nodes.add(new InverseSurrogateNeighbor(n));
				// }

				if (target.fold != NULL_NODE) {
					nodes.add(new Fold(target.fold));
				}
				if (target.sFold != NULL_NODE) {
					nodes.add(new SurrogateFold(target.sFold));
				}
				if (target.isFold != NULL_NODE) {
					nodes.add(new InverseSurrogateFold(target.isFold));
				}

				for (Node n : target.neighbors) {
					nodes.add(new Neighbor(n));
				}
				// nodes.addAll(neighbors);
				for (Node n : target.upPointers) {
					nodes.add(new InverseSurrogateNeighbor(n));
				}

				for (Node n : target.downPointers) {
					nodes.add(new SurrogateNeighbor(n));
				}

				return nodes.iterator();
			}

			@Override
			public void modify(Node node, Node target) {
				node.replace(target, replacingNode);
			}
			// replace
		}.execute();

		// Fix the changes of the States
		// leavingNode.changeStatesOfNodes();
		//
		// for (Node n : leavingNode.getAllConnections()) {
		// n.changeStatesOfNodes();
		// }
		this.webId = leavingNode.getWebId();
		// database.updateFold(this);
	}

	/**
	 * @author Robert Hickman
	 * @param node
	 * @return
	 */
	public boolean isHypercubeCapNode(Node node) {
		if ((node.getWebId().getValue() & (int) (Math.pow(2, node.getHeight()) - 1)) == (int) (Math
				.pow(2, node.getHeight()) - 1)
				&& node.getValue() == findZero().findBiggest().getValue()) {
			return true;
		}
		return false;
	}

	public ArrayList<Node> getAllConnections() {
		ArrayList<Node> combined = new ArrayList<Node>();
		combined.addAll(getNeighbors());
		combined.addAll(getUpPointers());
		combined.addAll(getDownPointers());
		if (fold.getWebId().getValue() != -1) {
			combined.add(fold);
		}
		if (sFold.getWebId().getValue() != -1) {
			combined.add(sFold);
		}
		if (isFold.getWebId().getValue() != -1) {
			combined.add(isFold);
		}
		return combined;
	}

	public Node findBiggest() {
		Node current = this;
		ArrayList<Node> combined = getAllConnections();
		for (Node n : combined) {
			if (n.getWebId().getValue() > current.getWebId().getValue()) {
				current = n;
			}
		}
		if (current.getValue() == this.getValue()) {
			return current;
		} else {
			return current.findBiggest();
		}
	}

	/**
	 * 
	 * @param id
	 * @author Robert Hickman
	 */
	public Node find(WebId id) {
		if (id.getValue() < 0
				|| id.getValue() > findZero().findBiggest().getValue()) {
			return NULL_NODE;
		}
		ArrayList<Node> combined = getAllConnections();
		int destinationId = id.getValue();
		int closestVal = this.getValue();
		if (closestVal == destinationId) {
			return this;
		}
		Node closest = this;
		int difference = Integer.bitCount(closestVal ^ destinationId);
		for (Node n : combined) {
			int currentDifference = Integer.bitCount(n.getValue()
					^ destinationId);
			if (currentDifference < difference
					|| (currentDifference == difference && Math.floor(Math
							.random() * 2) == 1)) {
				closest = n;
				difference = Integer.bitCount(closest.getValue()
						^ destinationId);
			}
		}
		return closest.find(id);
	}

	public Node findParent() {
		for (Node n : neighbors) {
			if (n.getWebId().getValue() == this.getWebId().getParentsValue()) {
				return n;
			}
		}
		return NULL_NODE;
	}

	private int createChildsWebId() {
		return getValue() | 1 << (getSpecialHeight() - 1);
	}

	/**
	 * @author Robert Hickman
	 * @return
	 */
	private Node findChild() {
		Node n = find(new WebId(createChildsWebId()));
		if (n.getValue() == getValue()) {
			return NULL_NODE;
		}
		return n;
		// if is a parent node

		// Less Old Code but doesn't work
		// if(this.upPointers.size()<1 && this.downPointers.size()<1 &&
		// this.neighbors.size()>0){
		// Node greatest=null;
		// //go through all its neighbors and return the node with the greatest
		// value
		// for (Node n : neighbors) {
		// if(greatest==null){
		// greatest=n;
		// }
		// else
		// {
		// if(n.getValue()>greatest.getValue()){
		// greatest=n;
		// }
		// }
		// }
		//
		// return greatest;
		// }
		// return NULL_NODE;
		// Old Code
		// for (Node n : neighbors) {
		// if (n.getValue() == (getValue() | 1 << (getHeight() - (childAdded ? 1
		// : 0)))) {
		// //
		// System.out.println("Parent: "+this.getValue()+" H: "+this.getHeight());
		// // System.out.println("Child: "+n.getValue());
		// return n;
		// }
		// }
		// return NULL_NODE;
	}

	/**
	 * @author Robert Hickman
	 * @return
	 */
	private LowerUpperBound findInsertionPoint() {
		/*
		 * Node tmp = findZero().findBiggest(); if (isHypercubeCapNode(tmp)) {
		 * return findZero(); } else { tmp = tmp.findParent(); return
		 * tmp.find(new WebId(tmp.getWebId().getValue() + 1)); }
		 */
		Node current = findBiggest();
		Node ret = null;
		Node lowerbound = NULL_NODE;
		Node upperbound = current;
		if (current.fold.getValue() == 0) {
			ret = current.fold;
		} else {
			boolean done = false;
			lowerbound = NULL_NODE;
			upperbound = current;
			int lowerVal = lowerbound.getValue();

			while (!done) {
				// lowerbound = find smallest surrogate neighbor
				for (Node n : upperbound.downPointers) {
					if (n.getValue() < lowerVal
							|| lowerbound.equals(NULL_NODE)) {
						lowerbound = n;
						lowerVal = lowerbound.getValue();
					}
				}
				if (upperbound.findParent().getValue() == lowerVal - 1) {
					ret = lowerbound;
					done = true;
				} else {
					int flag = 0;
					// if you have a larger inverse surrogate
					// upperbound = largest inverse surrogate neighbor
					int upperVal = upperbound.getValue();
					for (Node n : lowerbound.upPointers) {
						if (n.getValue() > upperVal) {
							upperbound = n;
							flag = 1;
							upperVal = upperbound.getValue();
						}
					}
					// else
					// lowerbound = smallest neighbor without a child
					// ??? neighbors + sNeighbors + isNeighbors ?
					if (flag == 0) {
						Node tmp = upperbound;
						int tVal = tmp.getValue();
						for (Node n : lowerbound.neighbors) {
							if (n.getValue() < tVal
									&& n.findChild().equals(NULL_NODE)) {
								tmp = n;
								tVal = tmp.getValue();
							}
						}
						lowerbound = tmp;
					}
				}
			}
		}
		return new LowerUpperBound(ret, upperbound);
	}

	private int getSpecialHeight() {
		int myHeight = this.webId.getHeight();

		for (Node n : this.getNeighbors()) {
			if (n.getWebId().getHeight() > myHeight) {
				myHeight = n.getWebId().getHeight();
			}
		}
		for (Node n : this.getUpPointers()) {
			if (n.getWebId().getHeight() > myHeight) {
				myHeight = n.getWebId().getHeight();
			}
		}
		for (Node n : this.getDownPointers()) {
			if (n.getWebId().getHeight() > myHeight) {
				myHeight = n.getWebId().getHeight();
			}
		}
		return myHeight;
	}

	public int getHeight() {
		return height.get();
		// int myHeight = this.webId.getHeight();
		//
		// for (Node n : this.getNeighbors()) {
		// if (n.getWebId().getHeight() > myHeight) {
		// myHeight = n.getWebId().getHeight();
		// }
		// }
		// /*
		// * for(Node n: this.getUpPointers()){
		// * if(n.getWebId().getHeight()>myHeight){
		// * myHeight=n.getWebId().getHeight(); } } for(Node n:
		// * this.getDownPointers()){ if(n.getWebId().getHeight()>myHeight){
		// * myHeight=n.getWebId().getHeight(); } }
		// */
		// return myHeight;
	}

	public int getValue() {
		return webId.getValue();
	}

	/**
	 * @author Robert Hickman
	 */
	public String ourToString() {
		String ret = "";
		ret += "WebID: " + getValue() + "\nNeighbors:";
		for (Node neighbor : neighbors) {
			ret += neighbor.getValue() + ",";
		}
		ret += "\nsNeighbors:";
		for (Node neighbor : downPointers) {
			ret += neighbor.getValue() + ",";
		}
		ret += "\nisNeighbors:";
		for (Node neighbor : upPointers) {
			ret += neighbor.getValue() + ",";
		}
		ret += "\nFold: " + fold.getValue() + "\nsFold: " + sFold.getValue();
		ret += "\nisFold: " + isFold.getValue() + "\n";
		return ret;
	}

	/**
	 * @author Thomas Holladay
	 */
	public void changeStatesOfNodes() {
		if (isTerminalNode()) {
			setState(new TerminalNodeState());
		} else if (downPointers.size() > 0) {
			setState(new DownPointingNodeState());
		} else if (upPointers.size() > 0) {
			setState(new UpPointingNodeState());
		} else if (isHypercubeCapNode(this)) {
			setState(new HypercubeCapState());
		} else {
			setState(new StandardNodeState());
		}
	}

	/**
	 * @author Robert Hickman
	 * @return
	 */
	private boolean isTerminalNode() {
		if (downPointers.size() > 0 && everyNeighborIsSmallerThan()) {
			return true;
		}
		return false;
	}

	/**
	 * @author Robert Hickman
	 * @return
	 */
	private boolean everyNeighborIsSmallerThan() {
		for (Node n : neighbors) {
			if (n.getValue() > getValue()) {
				return false;
			}
		}
		for (Node n : downPointers) {
			if (n.getValue() > getValue()) {
				return false;
			}
		}
		for (Node n : upPointers) {
			if (n.getValue() > getValue()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int compareTo(Node o) {
		return Integer.compare(webId.getValue(), o.getWebId().getValue());
	}

	public void clearConnections() {
		fold = NULL_NODE;
		sFold = NULL_NODE;
		isFold = NULL_NODE;
		height = new Height(0);
		upPointers = new ArrayList<Node>();
		downPointers = new ArrayList<Node>();
		neighbors = new ArrayList<Node>();
		state = new StandardNodeState();
	}

	@Override
	public String toString() {
		return (webId.getValue() == -1) ? "NULL_NODE" : "Node "
				+ webId.getValue() + " &" + System.identityHashCode(this);
	}

	private abstract class OperateOnConnections {
		protected Node target;

		public OperateOnConnections(Node target) {
			this.target = target;
		}

		public void execute() {
			Iterator<Node> appropriateNodesIterator = getIterator();
			while (appropriateNodesIterator.hasNext()) {
				modify(appropriateNodesIterator.next(), target);
			}
		}

		/**
		 * The getIterator in the insert and disconnect classes will iterate
		 * over neighbors, surrogateNeighbors and the Fold. The getIterator in
		 * the replace method iterates over all existing connections: neighbors,
		 * surrogate neighbors, fold, surrogate fold, and inverse surrogate
		 * fold.
		 * 
		 * @return an iterable of nodes for this instance
		 */
		public Iterator<Node> getIterator() {
			ArrayList<Node> nodes = new ArrayList<Node>();

			nodes.add(new Fold(fold));

			for (Node n : neighbors) {
				nodes.add(new Neighbor(n));
			}
			// nodes.addAll(neighbors);
			for (Node n : downPointers) {
				nodes.add(new SurrogateNeighbor(n));
			}
			return nodes.iterator();
		}

		public abstract void modify(Node node, Node target);
	}

	private class LowerUpperBound {
		public Node lower;
		public Node upper;

		public LowerUpperBound(Node l, Node u) {
			lower = l;
			upper = u;
		}
	}

	public void setMyContents(Contents myContents) {
		this.myContents = myContents;
	}

	// Get's this node's contents.
	public Contents getContents() {
		return myContents;
	}

	// The accept method for the visitor Pattern.
	public void accept(Visitor visitor, Parameters parameters) {
		visitor.visit(this, parameters);
	}

	private Object writeReplace() throws ObjectStreamException {
		System.out.println("Node writereplace");
		if (this.getValue() == -2) { //
			return this;
		} else {
			return new NodeProxy(globalObjectId);
		}
	}

	private Object readResolve() throws ObjectStreamException {
		System.out.println("Node readresolve");
		return this;
	}

	public LocalObjectId getLocalObjectId() {
		return globalObjectId.getLocalObjectId();
	}

	public void setGlobalObjectId(GlobalObjectId globalId) {
		globalObjectId = globalId;
	}
}
