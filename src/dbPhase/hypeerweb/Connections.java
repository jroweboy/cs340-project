package dbPhase.hypeerweb;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;

/**
 * This class wasn't mentioned specifically in the JavaDocs but was in some of
 * the method descriptions, it is used primarily to help facilitate database
 * manipulation.
 * 
 * <pre>
 * <b>Domain:</b>
 *     dbName        : File name for database.
 *     isLogged		 : Debug flag to turn logging on/off.
 *     
 *     <b>Invariant:</b>
 *     Runtime folder contains "sql.txt" that has a valid database creation 
 *     SQLite statement.
 * </pre>
 * 
 * @author Sam Grawe
 * 
 */

public class Connections {
	/**
	 * Local connection Object used throughout the class, primary object that
	 * interacts with the database.
	 */
	private Connection connection = null;
	/**
	 * Stored string for database file name.
	 */
	private String dbName;
	/**
	 * Internal string used to makes sure SQL statements are only executed once
	 * if passed to the database multiple times at once.
	 */
	private String previous = "";
	/**
	 * Internal flag used to clear log file at appropriate times.
	 */
	private boolean started = false;
	/**
	 * Internal count used to count the number of commits, primarily used in the
	 * log.
	 */
	private int updateCount = 1;
	/**
	 * Flag used to turn on/off logging.
	 */
	private boolean isLogged = true;

	/**
	 * Default Constructor.
	 * 
	 * @pre None.
	 * @post Connections is connected to default database file.
	 */
	public Connections() {
		dbName = "HyPeerWebDB.sqlite";
		connect();
	}

	/**
	 * Constructor used for custom database file.
	 * 
	 * @param dbName
	 *            File name for the database.
	 * @pre dbName is a valid filename, existent or not that System has
	 *      permissions to. If exists is a valid SQLite database.
	 * @post Connections is connected given database file.
	 */
	public Connections(String dbName) {
		this.dbName = dbName;
		connect();
	}

	/**
	 * Private internal method used to connect the program to the database.
	 * 
	 * @param dbName
	 *            File name of database. Created if it doesn't exist.
	 * @pre dbName is a valid filename, existent or not that System has
	 *      permissions to. If exists is a valid SQLite database.
	 * @post Connections is connected to given DB file.
	 */
	private void connect() {
		String riURL = "jdbc:sqlite:" + dbName;
		Statement stmnt;
		try {
			Class.forName("org.sqlite.JDBC");
			Properties prop = new Properties();
			prop.setProperty("PRAGMA foreign_keys", "ON");
			connection = DriverManager.getConnection(riURL, prop);
			stmnt = connection.createStatement();
			stmnt.execute("PRAGMA foreign_keys = ON;");
			stmnt.close();
			connection.setAutoCommit(false);
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Class used reset the database.
	 * 
	 * @pre Connections is connected to a database, the database isn't locked,
	 *      "sql.txt" is in runtime folder, contains a valid SQLite schema
	 *      statement and System has read permission for it.
	 * @post Database is empty and contains tables detailed in "sql.txt".
	 */
	public void clear() {
		try {
			connection.close();
			connect();
			updateLog("Flush Transactions");
			commit();
			updateLog("Clear Database");
			Statement stmnt;
			stmnt = connection.createStatement();
			stmnt.executeUpdate(getSchemea());
			stmnt.close();
			commit();
			started = false;
			updateCount = 1;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Internal class used to create initializing SQL statement sting from local
	 * file "sql.txt".
	 * 
	 * @return String the represents the SQL statement contained in "sql.txt".
	 * @pre File "sql.txt" is in runtime folder contains a valid SQLite schema
	 *      statement and System has read permission for it.
	 * @post SQL statement in "sql.txt" is in returned String object.
	 */
	private String getSchemea() {
		String ret = "";
		try {
			Scanner in = new Scanner(new BufferedReader(new FileReader(
					"sql.txt")));
			while (in.hasNextLine()) {
				ret = ret.concat(in.nextLine() + "\n");
			}
			in.close();
			return ret;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}
		return ret;
	}

	/**
	 * Used to set logging flag.
	 * 
	 * @param isDebugged
	 *            Boolean operator for logging flag, true==log, false==don't
	 *            log.
	 * @pre None.
	 * @post Connections will or will not log activity according to input.
	 */
	public void setDebugged(boolean isDebugged) {
		isLogged = isDebugged;
	}

	/**
	 * Closes database connection.
	 * 
	 * @pre Connections is connected to a database.
	 * @post Connections is no longer connected to a database.
	 */
	public void close() {
		updateLog("Close");
		this.commit();
		try {
			connection.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Commit current transaction to database.
	 * 
	 * @pre Connections is connected to a database.
	 * @post Current SQL transaction is committed to the database.
	 */
	public void commit() {
		try {
			connection.commit();
			previous = "";
			updateLog("Commit #" + updateCount++);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Execute given SQL query and return it's ResultSet.
	 * 
	 * @param sql
	 *            SQL query.
	 * @return ResultSet obtained from executing passing in query.
	 * @pre Database is not locked, sql is a valid SQL query.
	 * @post ResultSet contains the expected results of the SQL query.
	 */
	public ResultSet query(String sql) {
		Statement stmnt;
		ResultSet rs;
		try {
			stmnt = connection.createStatement();
			rs = stmnt.executeQuery(sql);
			updateLog(sql);
			return rs;
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * Add passed in SQL statement to transaction.
	 * 
	 * @param sql
	 *            SQL statement to be posted to current transaction.
	 * @pre Connections is connected to a database, the database is not locked,
	 *      and sql is a valid SQL statement that manipulates the DB.
	 * @post The current transaction contains sql's SQL statement.
	 */
	public void update(String sql) {
		Statement stmnt;
		try {
			if (!(previous.equalsIgnoreCase(sql))) {
				stmnt = connection.createStatement();
				updateLog(sql);
				stmnt.executeUpdate(sql);
				stmnt.close();
			}
			previous = sql;
		} catch (SQLException ex) {
			updateLog("SQLException");
			System.out.println(sql);
			ex.printStackTrace();
		}
	}

	/**
	 * Update logger. Internal class used to log all activity of Connections
	 * onto a file on disk.
	 * 
	 * @param log
	 *            String to be added to update log.
	 * @pre System has write access to "UpdateLog.txt" in the current folder.
	 * @post Input log is written to "UpdateLog.txt"
	 */
	private void updateLog(String log) {
		if (isLogged) {
			try {
				File file = new File("UpdateLog.txt");
				if (!file.exists()) {
					file.createNewFile();
				}
				BufferedWriter buff = new BufferedWriter(new FileWriter(file,
						started));
				if (!started) {
					started = true;
				}
				buff.write(log + "\n");
				buff.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Method used to obtain name of database file.
	 * 
	 * @return String object of the database filename.
	 * @pre dbName has been set via constructor.
	 * @post The current database name is contained in returned String.
	 */
	public String currentDB() {
		return dbName;
	}
}
