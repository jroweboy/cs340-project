package dbPhase.hypeerweb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Random;

import phase2.states.HypercubeCapState;
import phase2.states.StandardNodeState;
import phase2.testing.ExpectedResult;
import phase6.GlobalObjectId;
import phase6.LocalObjectId;
import phase6.MachineAddress;
import phase6.MyObserver;
import phase6.ObjectDB;
import phase6.PeerCommunicator;
import phase6.PortNumber;

/**
 * Stub for the HyPeerWeb class A graph of one or more nodes connected in such a
 * way as to support reliability and speedy information retrieval in a
 * peer-to-peer environment. For every node in the HyPeerWeb there is a copy of
 * that node in the database. Any changes to a node in the HyPeerWeb result in a
 * change in the corresponding node in the database. When the HyPeerWeb is
 * started, its nodes are loaded from the database. For Phase 1 many of the
 * HyPeerWeb constraints are relaxed. The HyPeerWeb is a singleton.
 * 
 * Domain: nodes : SetOf hypeerwebDB : HyPeerWebDatabase
 * 
 * Invariant: |nodes| ≥ 1 AND ∀ 0 ≤ i < |nodes| (∃ node (node ∈ nodes AND
 * node.webId.value = i))
 * 
 * -- In other words: -- There is a bijection from the webIds of the nodes to
 * the integers 0 to N-1.
 * 
 * @author James Rowe
 * 
 */
public class HyPeerWeb { // implements Serializable, Observer

	/**
	 * Gets the single HyPeerWeb, if one doesn't exist then one is created.
	 * 
	 * @return HyPeerWeb
	 */
	private Node nodeZero = null;
	private HyPeerWebDatabase database;
	private static HyPeerWeb singletonWeb;
	private static LocalObjectId localid;
	private static GlobalObjectId globalObjectId;
	private boolean dbCoupled = true;
	private ArrayList<gui.Main.HyPeerWeb> segments;
	private ArrayList<MyObserver> guis;
	static PortNumber port;

	public static HyPeerWeb getSingleton() {
		if (singletonWeb == null) {
			singletonWeb = new HyPeerWeb();
		}
		if (port == null) {
			Random rand = new Random();
			int min = 50000, max = 60000;

			int randomNum = rand.nextInt(max - min + 1) + min;
			port = new PortNumber(randomNum);
		}
		MachineAddress.setMachineAddress(GlobalObjectId.getMyIp());
		PortNumber.setApplicationsPortNumber(port);

		if (localid == null) {
			localid = new LocalObjectId(0);
		}
		globalObjectId = new GlobalObjectId(GlobalObjectId.getMyIp(), port,
				localid);
		ObjectDB.setFileLocation("main" + port + ".db");
		ObjectDB.getSingleton().restore(null);
		ObjectDB.getSingleton().store(globalObjectId.getLocalObjectId(),
				singletonWeb);
		// System.out.println("main() ObjectDB = ");
		// ObjectDB.getSingleton().dump();
		// System.out.println("GlobId: " + globalObjectId);
		return singletonWeb;
	}

	//
	// public static HyPeerWeb getSingleton(int local) {
	// localid = new LocalObjectId(local);
	// return getSingleton();
	// }

	/**
	 * Private constructor to make a
	 * 
	 * @param h
	 */
	private HyPeerWeb() {
		database = HyPeerWebDatabase.getSingleton();

		segments = new ArrayList<gui.Main.HyPeerWeb>();
		guis = new ArrayList<MyObserver>();

	}

	public void addSegment(GlobalObjectId globalId) {
		gui.Main.HyPeerWeb web = new gui.Main.HyPeerWeb(globalId);
		segments.add(web);
		// return web;
	}

	public void addSegmentToHyPeerWeb(GlobalObjectId globalId) {
		// gui.Main.HyPeerWeb newSegment = addSegment(globalId);
		gui.Main.HyPeerWeb newSegment = new gui.Main.HyPeerWeb(globalId);
		newSegment.addSegment(globalObjectId);
		for (gui.Main.HyPeerWeb s : segments) {
			s.addSegment(globalId);
			newSegment.addSegment(s.getGlobalObjectId());
		}
		addSegment(globalId);
		newSegment.setNodeZero(nodeZero);
		System.out.println("New connections established successfully");
	}
	
	public void setNodeZero(Node node){
		if (nodeZero == null && node != null) {
			database.add(node);
		} else if (node != null) {
			database.update(node);
		}
		nodeZero = node;
	}

	/**
	 * Adds a node to the HyPeerWeb
	 * 
	 * @param node
	 */
	public void addToHyPeerWeb(Node node) {
		if (node == null) {
			System.err.println("Cannot add a null node to the hypeerweb");
			return;
		}
		addToHyPeerWeb(node, nodeZero);
	}

	/**
	 * Adds a node to the HyPeerWeb
	 * 
	 * @author James Rowe
	 * @param startNode
	 */
	public void addToHyPeerWeb(Node node, Node startNode) {
		if (node == null) {
			System.err.println("Cannot add a null node to the hypeerweb");
			return;
		}
		// Node node = new Node(innode);
		// System.out.println("Adding a node");
		GlobalObjectId globalId = new GlobalObjectId(GlobalObjectId.getMyIp(),
				port, new LocalObjectId());
		node.setGlobalObjectId(globalId);
		//if(node.getClass()!=NodeProxy.class){
			ObjectDB.getSingleton().store(node.getLocalObjectId(), node);
		//}
		if (nodeZero == null) {
			// System.out.println("New Hypeerweb started");
			node.setWebId(new WebId(0));
			notifyAddToSegments(node);
			nodeZero = node;
			nodeZero.setState(new HypercubeCapState());
			database.add(nodeZero);
		} else {
			WebId webId = node.getWebId();
			int newWebId;
			if (webId.getValue() > 0) {
				newWebId = nodeZero.findZero().findBiggest().getWebId()
						.getValue() + 1;
			} else {
				newWebId = nodeZero.findBiggest().getWebId().getValue() + 1;
			}
			node.setWebId(new WebId(newWebId));
			notifyAddToSegments(node);
			if (nodeZero.getNeighbors().size() < 1) {
				node.setWebId(new WebId(newWebId));
				notifyAddToSegments(node);
				// ystem.out.println("adding second node. "+nodeZero+" "+node);
				nodeZero.addNeighbor(node);
				node.addNeighbor(nodeZero);
				node.setFold(nodeZero);
				nodeZero.setFold(node);
				nodeZero.setState(new StandardNodeState());
				node.setState(new HypercubeCapState());
				database.addQueue(node);
				database.addNeighborQueue(node, nodeZero, "n");
				database.update(nodeZero);
				database.commit();
			} else {
				// node.setWebId();
				getNode(startNode.getValue()).addToHyPeerWeb(node);
			}
		}

		//verify();

		notifyObservers("add");

		// setChanged();
		// notifyObservers(this.size());
		// // UpdateDatabase();
		// if (startNode != null) {
		// startNode.updateDatabase(node.getWebId(), database);
		// } else {
		// nodeZero.updateDatabase(node.getWebId(), database);
		// }
		// saveToDatabase();
	}

	/**
	 * Clears the HyPeerWeb
	 */
	public void clear() {
		nodeZero = null;
		if (dbCoupled) {
			database.clear();
		}
	}

	/**
	 * Determines whether the indicated node is in the HyPeerWeb.
	 * 
	 * @author James Rowe
	 * @param node
	 * @return
	 */
	public boolean contains(Node node) {
		return node.getValue() >= 0 && node.getValue() < size();
	}

	public boolean contains(int id) {
		return id >= 0 && id < size();
	}

	/**
	 * Returns the single HyPeerWeb Database.
	 * 
	 * @return
	 */
	public HyPeerWebDatabase getHyPeerWebDatabase() {
		return database;
	}

	public void setDBCoupling(boolean isCoupled) {
		dbCoupled = isCoupled;
	}

	public boolean getDBCoupling() {
		return dbCoupled;
	}

	/**
	 * Gets the node with the indicated index i.
	 * 
	 * @author Robert Hickman
	 */
	public Node getNode(int i) {
		if (i >= this.size()) {
			return Node.NULL_NODE;
		}
		if (nodeZero == null) {
			return Node.NULL_NODE;
		}
		if (i == 0) {
			return nodeZero;
		}
		return nodeZero.find(new WebId(i));
	}

	/**
	 * Reloads the HyPeerWeb from the database using the default database name.
	 */
	public void reload() {
		reload(null);
	}

	/**
	 * Reloads the HyPeerWeb from the database using dbName.
	 * 
	 * @author James Rowe
	 * @param dbName
	 */
	public void reload(String dbName) {
		System.out.println("  In Reload");
		clear();
		ArrayList<Node> nodes;
		if (dbName == null) {
			nodes = database.getNodesFromDataBase();
		} else {
			nodes = database.getNodesFromDataBase(dbName);
		}
		Collections.sort(nodes);
		System.out.println("  Got Nodes");
		if (nodes.size() == 0) {
			nodeZero = null;
		} else {
			nodeZero = nodes.get(0);
		}
	}

	/**
	 * Removes a node from the HyPeerWeb
	 * 
	 * @param node
	 */
	public void removeNode(Node node) {
		Node nodeToRemove = getNode(node.getValue());
		Node possiblyNewNodeZero = null;
		ObjectDB.getSingleton().remove(node.getLocalObjectId());
		if (nodeToRemove.equals(nodeZero)) {
			possiblyNewNodeZero = nodeZero.findBiggest();
		}
		int nodeDelete = nodeZero.removeFromHyPeerWeb(nodeToRemove);
		notifyRemoveToSegments(nodeDelete, node.getValue());
		if (possiblyNewNodeZero != null) {
			nodeZero = possiblyNewNodeZero;
		}
		notifyObservers("remove");

		// for (int i = 0; i < size(); i++) {
		// getNode(i).changeStatesOfNodes();
		// }

		// saveToDatabase();
	}

	/**
	 * Saves all the nodes in the HyPeerWeb and their corresponding information
	 * in the database.
	 */
	public void saveToDatabase() {
		ArrayList<Node> list = new ArrayList<Node>();
		for (int i = 0; i < size(); i++) {
			list.add(getNode(i));
		}
		database.saveHyPeerWeb(list.toArray(new Node[size()]));
	}

	@Override
	public String toString() {
		StringBuilder a = new StringBuilder();
		a.append("Nodes- ");
		int i = 0;
		for (i = 0; i < size(); i++) {
			// a.append(getNode(i).ourToString() + "\n");
			a.append(getNode(i).getValue() + " "); // .toString());
			// a.append(i+"@"+System.identityHashCode(getNode(i))+ " ");
		}
		// never looped
		if (i == 0) {
			a.append("Empty");
		}
		return a.toString();
	}

	public void verify() {
		for (int i = 0; i < size(); i++) {
			verifyNode(getNode(i));
		}
	}

	public void verifyNode(Node node) {
		ExpectedResult expected = new ExpectedResult(this.size(),
				node.getValue());
		SimplifiedNodeDomain simplifiedNodeDomain = node
				.constructSimplifiedNodeDomain();
		if (!simplifiedNodeDomain.equals(expected)) {
			printErrorMessage(simplifiedNodeDomain, expected);
		}
	}

	private static void printErrorMessage(
			SimplifiedNodeDomain simplifiedNodeDomain,
			ExpectedResult expectedResult) {

		System.out.println("Node Information: \t\t\tExpected Information");
		// splitting up the expected info to make it two column
		String[] sd = simplifiedNodeDomain.toString().split("\n");
		String[] er = expectedResult.toString().split("\n");
		for (int i = 0; i < sd.length; ++i) {
			String b = (i == (sd.length - 1)) ? er[i] : er[i].substring(24);
			System.out.println(sd[i] + "\t\t" + b);
		}
	}

	/**
	 * @author Robert Hickman
	 * @return
	 */
	public int size() {
		if (nodeZero == null) {
			return 0;
		} else {
			return nodeZero.findBiggest().getWebId().getValue() + 1;
		}
	}

	// SAM FILL IN THESE TWO METHODS
	public void notifiedRemove(Object removed, Object replacing) {
		database.deleteReplaceNeighbor((Integer) removed, (Integer) replacing);
	}

	public void notifiedAdd(Object node) {
		if (nodeZero == null) {
			nodeZero = (NodeProxy) node;
		}
		
		database.add((Node) node);
	}

	public void notifyRemoveToSegments(Object removed, Object replacing) {

		for (gui.Main.HyPeerWeb hi : segments) {
			hi.notifiedRemove(removed, replacing);
		}
	}

	public void notifyAddToSegments(Object node) {
		// now tell all the other segments about the changes
		for (gui.Main.HyPeerWeb hi : segments) {
			hi.notifiedAdd(node);
		}
	}

	public void notifyOnce(String a) {
		Object[] arrLocal;
		synchronized (this) {
			arrLocal = guis.toArray();
		}

		for (int i = arrLocal.length - 1; i >= 0; i--) {
			((MyObserver) arrLocal[i]).update(a);
		}
	}

	/**
	 * Called when a node is either added or deleted.
	 * 
	 * If a node was added, param a is the WebId of the node and param b will be
	 * the NULL_NODE
	 * 
	 * If a node is being removed params a and b are the insertion point and the
	 * 
	 * @param a
	 * @param b
	 */
	public void notifyObservers(String a) {
		Object[] arrLocal;
		synchronized (this) {
			arrLocal = guis.toArray();
		}

		for (int i = arrLocal.length - 1; i >= 0; i--) {
			((MyObserver) arrLocal[i]).update(a);
		}

		// now tell all the other segments about the changes
		for (gui.Main.HyPeerWeb hi : segments) {
			hi.notifyOnce(a);
		}
	}

	public void addObserver(MyObserver m) {
		for (MyObserver each : guis) {
			if (each.equals(m)) {
				return;
			}
		}
		guis.add(m);
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		try {
			port = new PortNumber(Integer.parseInt(args[0]));
		} catch (Exception e) {
			Random rand = new Random();
			int min = 50000, max = 60000;

			int randomNum = rand.nextInt(max - min + 1) + min;
			port = new PortNumber(randomNum);
		}
		System.out.println("Port: " + port);
		final HyPeerWeb h = HyPeerWeb.getSingleton();
		PeerCommunicator.createPeerCommunicator(port);

		try {
			if (args[1].contains(":")) {
				String ip = args[1].split(":")[0];
				String o_port = args[1].split(":")[1];
				GlobalObjectId peer = new GlobalObjectId(ip, new PortNumber(
						Integer.parseInt(o_port)), new LocalObjectId(0));
				gui.Main.HyPeerWeb remote = new gui.Main.HyPeerWeb(peer);
				remote.addSegmentToHyPeerWeb(globalObjectId);

			}
		} catch (Exception e) {
		}
		// final GlobalObjectId globalObjectId = new GlobalObjectId();
		System.out.println("Waiting for connections.... and stuff...");

		// EventQueue.invokeLater(h);
		// ExecutorService exec = Executors.newSingleThreadExecutor();
		// exec.submit(h);
	}

	public ArrayList<Node> getMyNodes() {
		ArrayList<Node> myNodes = new ArrayList<Node>();
		for (Enumeration<Object> e = ObjectDB.getSingleton().enumeration(); e
				.hasMoreElements();) {
			Object current = e.nextElement();
			if (current.getClass() == Node.class) {
				Node node = (Node) current;
				if(globalObjectId.onSameMachineAs(node.getGlobalId())){
					myNodes.add(node);
				}
			}
		}
		return myNodes;
	}
	//
	// @Override
	// public void update(Observable arg0, Object arg1) {
	// // Notify all of my GUIs that something has changed also
	// int newSize = (Integer) arg1;
	// if (newSize != size()) {
	// // if I'm not getting a message that I've already received
	// hasChanged();
	// notifyObservers(newSize);
	// }
	// }

	//
	// @Override
	// public void run() {
	// int i = 0;
	// while (true) {
	// System.out.println(".");
	//
	// setChanged();
	// notifyObservers();
	// }
	// }
}
