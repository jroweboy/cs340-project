package dbPhase.hypeerweb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import phase2.states.NodeState;
import phase4.classes.Contents;
import phase6.GlobalObjectId;
import phase6.LocalObjectId;
import phase6.PortNumber;

/**
 * HyPeerWebDatabase class.
 * 
 * @author Sam Grawe
 */
public class HyPeerWebDatabase {

	/**
	 * Null database flag, set true if null database wants to be used.
	 */

	/**
	 * Database singleton object.
	 */
	private static HyPeerWebDatabase singletonDatabase;

	/**
	 * Connection object.
	 */
	private Connections connection;

	/**
	 * Internal flag used to toggle dynamic updating of the database primarily
	 * used when adding or removing Nodes.
	 */
	private boolean isDynamic = true;

	/**
	 * Constructor.
	 */
	private HyPeerWebDatabase() {
		connection = new Connections("Database" + HyPeerWeb.port + ".sqlite");
		connection.clear();
	}

	/**
	 * Use is obvious.
	 * 
	 * @return Current Dynamic setting
	 */
	public boolean isDynamic() {
		return isDynamic;
	}

	/**
	 * Set database Dynamic flag.
	 * 
	 * @param newDynamic
	 */
	// public void setDynamic(boolean newDynamic) {
	// isDynamic = newDynamic;
	// }

	/**
	 * Creates a SimplifiedNodeDomain representing the node with indicated
	 * webId.
	 * 
	 * @param webID
	 *            WebID of given Node!
	 * 
	 * @return SDN of Node
	 */
	public SimplifiedNodeDomain getNode(final int webID) {
		int height = -1;
		int fold = -1;
		int surrogateFold = -1;
		int inverseSurrogateFold = -1;
		int state = -1;
		ResultSet nodeRS = connection
				.query("Select * from node where web_id = " + webID);
		try {
			if (nodeRS.next()) {
				height = nodeRS.getInt("height");
				state = nodeRS.getInt("state");
				fold = nodeRS.getInt("fold");
				surrogateFold = nodeRS.getInt("s_fold");
				inverseSurrogateFold = nodeRS.getInt("is_fold");
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		HashSet<Integer> neighbors = getNeighbor(webID, "n");
		HashSet<Integer> upPointers = getISNeighbors(webID);
		HashSet<Integer> downPointers = getNeighbor(webID, "sn");

		return new SimplifiedNodeDomain(webID, height, neighbors, upPointers,
				downPointers, fold, surrogateFold, inverseSurrogateFold, state);
	}

	/**
	 * Get inverse surrogate neighbors of given webID.
	 * 
	 * @param webID
	 *            In webID for node.
	 * @return Array of IS neighbors of given node.
	 */
	private HashSet<Integer> getISNeighbors(final int webID) {
		HashSet<Integer> isNeighbors = new HashSet<>();
		ResultSet rs = connection
				.query("Select * from neighbor where type = 'sn' and neighbor = "
						+ webID);
		try {
			while (rs.next()) {
				isNeighbors.add(rs.getInt("web_id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isNeighbors;
	}

	/**
	 * Get (surrogate) neighbors of given webID.
	 * 
	 * @param webID
	 *            Input WebID
	 * @param type
	 *            Neighbor type ('n' for neighbors, 'sn' for surrogate
	 *            neighbors)
	 * @return Array of (Surrogate) neighbors
	 */
	private HashSet<Integer> getNeighbor(final int webID, final String type) {
		HashSet<Integer> neighbors = new HashSet<>();
		ResultSet rs = connection.query("Select * from neighbor where type = '"
				+ type + "'" + "and web_id = " + webID);
		try {
			while (rs.next()) {
				neighbors.add(rs.getInt("neighbor"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return neighbors;
	}

	/**
	 * Gets the single HyPeerWebDatabase.
	 * 
	 * @return Returns an instance of the database
	 */
	public static HyPeerWebDatabase getSingleton() {
		// if(isNullDB){
		// return NULL_DB;
		// }
		if (singletonDatabase == null) {
			singletonDatabase = new HyPeerWebDatabase();
		}
		return singletonDatabase;
	}

	/**
	 * Creates and initializes HyPeerWebDatabase with the given name.
	 * 
	 * @param dbName
	 *            Custom DB Name
	 */
	public void initHyPeerWebDatabase(final String dbName) {
		connection = new Connections(dbName);
		connection.clear();
		connection.commit();
	}

	/**
	 * Commit the array of Nodes to DB.
	 * 
	 * @param setOfNodes
	 *            Input HyPeerWeb as a set of Nodes.
	 */
	public void saveHyPeerWeb(final Node[] setOfNodes) {
		connection.clear();
		for (Node n : setOfNodes) {
			addQueue(n);
		}
		connection.commit();
	}

	/**
	 * Add a node to the database and commit transaction.
	 * 
	 * @param n
	 *            Input Node!
	 */
	public void add(final Node n) {
		addQueue(n);
		connection.commit();
	}

	/**
	 * Delete a node from the database transaction.
	 * 
	 * @param n
	 */
	public void delete(Node n) {
		File deleteMe = new File(genFileName(n));
		if (deleteMe.exists()) {
			deleteMe.delete();
		}
		String sql = "DELETE FROM node WHERE web_id = " + n.getValue() + ";";
		connection.update(sql);
	}

	/**
	 * Internal class used to generate name for serialized content.
	 * 
	 * @param n
	 *            Node with content to be saved to HDD.
	 * @return
	 */
	private String genFileName(Node n) {
		return connection.currentDB() + "Content" + n.getValue() + ".ser";
	}

	/**
	 * Loads sql inserts into DB transaction.
	 * 
	 * @param n
	 *            Input node.
	 */
	public void addQueue(final Node n) {
		String sql;
		if (n instanceof NodeProxy) {
			String machine;
			if (n.getGlobalId().getMachineAddr() == null) {
				machine = "Null Machine";
			} else {
				machine = n.getGlobalId().getMachineAddr();
			}
			sql = "insert into node values(" + n.getValue()
					+ ", null, null, null, null, null, null, 'proxy'" + ", '"
					+ machine + "'" + ", "
					+ n.getGlobalId().getPortNumber().getValue() + ", "
					+ n.getGlobalId().getLocalObjectId().getId() + ");";
		} else {
			StringBuilder presql = new StringBuilder("insert into node values("
					+ n.getWebId().getValue() + "," + n.getHeight() + ","
					+ n.getFold().getValue() + "," + n.getSFold().getValue()
					+ "," + n.getISFold().getValue() + ","
					+ n.getState().getNodeState() + ", ");
			if (n.getContents() != null) {
				String fileName = genFileName(n);
				try {
					FileOutputStream fileOut = new FileOutputStream(fileName);
					ObjectOutputStream out = new ObjectOutputStream(fileOut);
					out.writeObject(n.getContents());
					out.close();
					fileOut.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
				presql.append("'" + fileName + "'");
			} else {
				presql.append("null");
			}
			GlobalObjectId globalId = n.getGlobalId();
			String machine = globalId.getMachineAddr();
			int port;
			if (globalId.getPortNumber() != null) {
				port = globalId.getPortNumber().getValue();
			} else {
				port = -1;
			}
			presql.append(", 'node', '" + machine + "', " + port + ", "
					+ globalId.getLocalObjectId().getId());
			presql.append(");");
			sql = presql.toString();
			for (Node node : n.getNeighbors()) {
				insertNeighbor(n.getWebId(), node.getWebId(), "n");
			}
			for (Node node : n.getDownPointers()) {
				insertNeighbor(n.getWebId(), node.getWebId(), "sn");
			}
		}
		connection.update(sql);
	}

	public void addNeighbor(Node node1, Node neighbor, String type) {
		addNeighborQueue(node1, neighbor, type);
		connection.commit();
	}

	public void addNeighborQueue(Node node1, Node neighbor, String type) {
		insertNeighbor(node1.getWebId(), neighbor.getWebId(), type);
	}

	public void removeNeighborQueue(Node node1, Node neighbor) {
		String sql = "DELETE from neighbor where web_id = " + node1.getValue()
				+ " and neighbor = " + neighbor.getValue() + ";";
		connection.update(sql);
	}

	public void update(Node node) {
		if (node.getValue() > -1) {
			String sql = "";
			if (node instanceof NodeProxy) {
				sql = "UPDATE node SET fold = null" + ", s_fold = null"
						+ ", state = null" + ", height = null"
						+ ", is_fold = null" + ", node_type = 'proxy'"
						+ ", machine_addr = null" + ", port = "
						+ node.getGlobalId().getPortNumber().getValue()
						+ ", local_id = "
						+ node.getGlobalId().getLocalObjectId().getId()
						+ " WHERE web_id = " + node.getValue() + ";";
			} else {
				GlobalObjectId globalId = node.getGlobalId();
				int fold = node.getFold().getValue();
				int sFold = node.getSFold().getValue();
				String machine = globalId.getMachineAddr();
				int port;
				if (globalId.getPortNumber() != null) {
					port = globalId.getPortNumber().getValue();
				} else {
					port = -1;
				}
				sql = "UPDATE node SET fold = " + fold + ", s_fold = " + sFold
						+ ", state = " + node.getState().getNodeState()
						+ ", height = " + node.height.get() + ", is_fold = "
						+ node.getISFold().getValue() + ", node_type = 'node'"
						+ ", machine_addr = '" + machine + "'" + ", port = "
						+ port + ", local_id = "
						+ node.getGlobalId().getLocalObjectId().getId()
						+ " WHERE web_id = " + node.getValue() + ";";
			}
			connection.update(sql);
		}
	}

	public void commit() {
		connection.commit();
	}

	/**
	 * Insert a neighbor relation.
	 * 
	 * @param webId1
	 *            Main webID
	 * @param webId2
	 *            WebID neighbor
	 * @param type
	 *            Type of neighbor
	 */
	private void insertNeighbor(final WebId webId1, final WebId webId2,
			final String type) {
		connection.update("INSERT INTO neighbor VALUES(" + webId1.getValue()
				+ "," + webId2.getValue() + ",'" + type + "');");
	}

	/**
	 * Reconstruct HyPeerWeb.
	 * 
	 * @return List of the HyPeerWeb Nodes
	 */
	public ArrayList<Node> getNodesFromDataBase() {
		isDynamic = false;
		ArrayList<Node> nodes = new ArrayList<>();
		try {
			ResultSet check = connection.query("select * from sqlite_master");
			if (check.isBeforeFirst()) {
				ResultSet baseNodes = connection.query("Select * from node");
				while (baseNodes.next()) {
					Node temp;
					GlobalObjectId globalId = new GlobalObjectId(
							baseNodes.getString("machine_addr"),
							new PortNumber(baseNodes.getInt("port")),
							new LocalObjectId(baseNodes.getInt("local_id")));
					if (baseNodes.getString("node_type").equals("node")) {
						temp = new Node(baseNodes.getInt("web_id"),
								baseNodes.getInt("height"));
						temp.setDynamic(false);
						temp.setState(NodeState.getNodeState(baseNodes
								.getInt("state")));
						String fileName = baseNodes.getString("contents");
						if (fileName != null) {
							try {
								FileInputStream fileIn = new FileInputStream(
										fileName);
								ObjectInputStream in = new ObjectInputStream(
										fileIn);
								Contents contents = (Contents) in.readObject();
								in.close();
								fileIn.close();
								temp.setMyContents(contents);
							} catch (IOException ex) {
								ex.printStackTrace();
							} catch (ClassNotFoundException ex) {
								ex.printStackTrace();
							}
						}
						temp.setGlobalObjectId(globalId);
					} else {
						temp = new NodeProxy(globalId);
					}
					nodes.add(temp);
				}
				ResultSet folds = connection.query("Select * from node");
				while (folds.next()) {
					Node temp = Node.NULL_NODE;
					Node fold = Node.NULL_NODE;
					Node sFold = Node.NULL_NODE;
					Node isFold = Node.NULL_NODE;
					for (Node n : nodes) {
						if (folds.getInt("web_id") == n.getValue()) {
							temp = n;
						}
						if (folds.getInt("fold") == n.getValue()) {
							fold = n;
						}
						if (folds.getInt("s_fold") == n.getValue()) {
							sFold = n;
						}
						if (folds.getInt("is_fold") == n.getValue()) {
							isFold = n;
						}
					}
					if (folds.getString("node_type").equals("node")) {
						temp.setFold(fold);
						temp.setSurrogateFold(sFold);
						temp.setInverseSurrogateFold(isFold);
					}
				}
				ResultSet neighbors = connection
						.query("Select * from neighbor;");
				while (neighbors.next()) {
					Node temp = Node.NULL_NODE;
					Node neighbor = Node.NULL_NODE;
					for (Node n : nodes) {
						if (neighbors.getInt("web_id") == n.getWebId()
								.getValue()) {
							temp = n;
						}
						if (neighbors.getInt("neighbor") == n.getWebId()
								.getValue()) {
							neighbor = n;
						}
					}
					if (neighbors.getString("type").equals("n")) {
						temp.addNeighbor(neighbor);
					} else {
						temp.addDownPointer(neighbor);
						neighbor.addUpPointer(temp);
					}
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (Node n : nodes) {
			n.setDynamic(true);
		}
		isDynamic = true;
		return nodes;
	}

	/**
	 * Connects to non-default database to load it's HyPeerWeb.
	 * 
	 * @param dbName
	 *            Name of custom database
	 * @return Nodes in the database
	 */
	public ArrayList<Node> getNodesFromDataBase(final String dbName) {
		connection.close();
		connection = new Connections(dbName);
		ArrayList<Node> ret = getNodesFromDataBase();
		connection = new Connections();
		return ret;
	}

	/**
	 * Get Neighbors.
	 * 
	 * @param n
	 *            input Node
	 * @return Returns a list of it's neighbors
	 */
	public ArrayList<Node> getNeighborsFromDataBase(final Node n) {
		return getNeighbors(n.getWebId().getValue(), "n");
	}

	/**
	 * Get Surrogate Neighbors.
	 * 
	 * @param n
	 *            input Node
	 * @return List of surrogate neighbors
	 */
	public ArrayList<Node> getSurrogateNeighborsFromDataBase(final Node n) {
		return getNeighbors(n.getWebId().getValue(), "sn");
	}

	/**
	 * Get inverse surrogate neighbors.
	 * 
	 * @param n
	 *            Input node.
	 * @return Returnn a list of isNeighbors.
	 */
	public ArrayList<Node> getInverseSurrogateNeighborsFromDataBase(final Node n) {
		ArrayList<Node> isNeighbors = new ArrayList<>();
		ResultSet rs = connection
				.query("Select * from neighbor where type = 'sn' and neighbor = "
						+ n.getWebId().getValue());
		try {
			while (rs.next()) {
				isNeighbors.add(new Node(rs.getInt("web_id")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return isNeighbors;
	}

	/**
	 * Sub class for neighbor retrieval.
	 * 
	 * @param webID
	 *            Input WebID
	 * @param type
	 *            Neighbor type to retrieve.
	 * @return Returns the neighbors.
	 */
	private ArrayList<Node> getNeighbors(final int webID, final String type) {
		ArrayList<Node> neighbors = new ArrayList<>();
		ResultSet rs = connection.query("Select * from neighbor where type = '"
				+ type + "'" + "and web_id = " + webID);
		try {
			while (rs.next()) {
				neighbors.add(new Node(rs.getInt("neighbor")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return neighbors;
	}

	/**
	 * Empties out Database.
	 */
	public void clear() {
		File directory = new File(".");
		File[] files = directory.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().startsWith(connection.currentDB())
					&& files[i].getName().endsWith(".ser")) {
				files[i].delete();
			}
		}
		connection.clear();
	}

	public void deleteReplace(Node node, int toReplace) {
		delete(node);
		if (toReplace != node.getValue()) {
			String sql = "UPDATE node SET web_id = " + node.getValue()
					+ " WHERE web_id = " + toReplace + ";"
					+ "\nUPDATE node SET fold = " + node.getValue()
					+ " WHERE fold = " + toReplace + ";"
					+ "\nUPDATE node SET s_fold = " + node.getValue()
					+ " WHERE s_fold = " + toReplace + ";"
					+ "\nUPDATE node SET is_fold = " + node.getValue()
					+ " WHERE is_fold = " + toReplace + ";"
					+ "\nUPDATE neighbor SET web_id = " + node.getValue()
					+ " WHERE web_id = " + toReplace + ";"
					+ "\nUPDATE neighbor SET neighbor = " + node.getValue()
					+ " WHERE neighbor = " + toReplace + ";";
			connection.update(sql);
			Node temp = new Node(toReplace);
			File renameMe = new File(genFileName(temp));
			if (renameMe.exists()) {
				renameMe.renameTo(new File(genFileName(node)));
				sql = "UPDATE node SET contents = '" + genFileName(node) + "'"
						+ " WHERE contents = '" + genFileName(temp) + "';";
				connection.update(sql);
			}
		}
	}

	public void deleteReplaceNeighbor(Integer node, Integer toReplace) {
		delete(new Node(node));
		if (node != toReplace) {
			String sql = "UPDATE node SET web_id = " + node
					+ " WHERE web_id = " + toReplace + ";"
					+ "\nUPDATE neighbor SET web_id = " + node
					+ " WHERE web_id = " + toReplace + ";"
					+ "\nUPDATE neighbor SET neighbor = " + node
					+ " WHERE neighbor = " + toReplace + ";";
			connection.update(sql);
			connection.commit();
		}
	}

	public String getDBName() {
		return connection.currentDB();
	}

	/**
	 * Null HyPeerWebDatabase, made after Null object pattern.
	 */
	public static final HyPeerWebDatabase NULL_DB = new HyPeerWebDatabase() {
		@Override
		public boolean isDynamic() {
			return false;
		}

		@Override
		public SimplifiedNodeDomain getNode(final int webID) {
			return null;
		}

		@Override
		public void initHyPeerWebDatabase(final String dbName) {
		}

		@Override
		public void saveHyPeerWeb(final Node[] setOfNodes) {
		}

		@Override
		public void add(final Node n) {
		}

		@Override
		public void delete(Node n) {
		}

		@Override
		public void addQueue(final Node n) {
		}

		@Override
		public void addNeighbor(Node node1, Node neighbor, String type) {
		}

		@Override
		public void addNeighborQueue(Node node1, Node neighbor, String type) {
		}

		@Override
		public void removeNeighborQueue(Node node1, Node neighbor) {
		}

		@Override
		public void update(Node node) {
		}

		@Override
		public void commit() {
		}

		@Override
		public ArrayList<Node> getNodesFromDataBase() {
			ArrayList<Node> nodes = new ArrayList<>();
			return nodes;
		}

		@Override
		public ArrayList<Node> getNodesFromDataBase(final String dbName) {
			ArrayList<Node> ret = getNodesFromDataBase();
			return ret;
		}

		@Override
		public ArrayList<Node> getNeighborsFromDataBase(final Node n) {
			return new ArrayList<Node>();
		}

		@Override
		public ArrayList<Node> getSurrogateNeighborsFromDataBase(final Node n) {
			return new ArrayList<Node>();
		}

		@Override
		public ArrayList<Node> getInverseSurrogateNeighborsFromDataBase(
				final Node n) {
			ArrayList<Node> isNeighbors = new ArrayList<>();
			return isNeighbors;
		}

		@Override
		public void clear() {
		}

		@Override
		public void deleteReplace(Node node, int toReplace) {
		}
	};
}
