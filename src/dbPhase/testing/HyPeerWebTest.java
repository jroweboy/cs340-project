package dbPhase.testing;

import junit.framework.TestCase;
import dbPhase.hypeerweb.HyPeerWeb;
import dbPhase.hypeerweb.Node;

public class HyPeerWebTest extends TestCase {

	private HyPeerWeb test = HyPeerWeb.getSingleton();

	public HyPeerWebTest(final String s) {
		super(s);
	}

	public void testClear() {
		test.addToHyPeerWeb(new Node(0));

		test.clear();

		assertEquals(test.size(), 0);

	}

	public void testAddNode() {
		Node n1 = new Node(0);
		Node n2 = new Node(1);
		Node n3 = new Node(2);
		Node n4 = new Node(3);

		test.addToHyPeerWeb(n1);
		test.addToHyPeerWeb(n2);
		test.addToHyPeerWeb(n3);

		assertTrue(test.contains(n1));
		assertTrue(test.contains(n2));
		assertTrue(test.contains(n3));

		assertFalse(test.contains(n4));
	}

	public void testSize() {
		test.clear();

		test.addToHyPeerWeb(new Node(0));

		assertEquals(test.size(), 1);

		test.addToHyPeerWeb(new Node(1));
		test.addToHyPeerWeb(new Node(2));

		assertEquals(test.size(), 3);

		test.clear();

		assertEquals(test.size(), 0);
	}

	public void testGetNode() {
		Node n1 = new Node(0);
		Node n2 = new Node(1);
		Node n3 = new Node(2);
		@SuppressWarnings("unused")
		Node n4 = new Node(3);

		test.addToHyPeerWeb(n1);
		test.addToHyPeerWeb(n2);
		test.addToHyPeerWeb(n3);

		assertEquals(test.getNode(0), n1);
		assertEquals(test.getNode(1), n2);
		assertEquals(test.getNode(2), n3);
		assertEquals(test.getNode(3), null);
	}

}
