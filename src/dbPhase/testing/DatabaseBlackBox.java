package dbPhase.testing;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

import junit.framework.TestCase;
import phase4.classes.Contents;
import phase6.GlobalObjectId;
import phase6.LocalObjectId;
import phase6.PortNumber;
import dbPhase.hypeerweb.HyPeerWebDatabase;
import dbPhase.hypeerweb.Node;
import dbPhase.hypeerweb.NodeProxy;
import dbPhase.hypeerweb.SimplifiedNodeDomain;

public class DatabaseBlackBox extends TestCase {

	public DatabaseBlackBox(String name) {
		super(name);
	}

	public void testIsDynamic() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		assertTrue(database.isDynamic());
	}

	public void testGetNodeAndAdd() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		database.add(new Node(0));
		SimplifiedNodeDomain sndExpected = new Node(0)
				.constructSimplifiedNodeDomain();
		SimplifiedNodeDomain sndGot = database.getNode(0);
		assertTrue(sndExpected.toString().equals(sndGot.toString()));
		assertTrue(null == database.getNode(1));
		assertTrue(null == database.getNode(Integer.MAX_VALUE / 2));
		assertTrue(null == database.getNode(-1));
		assertTrue(null == database.getNode(Integer.MIN_VALUE / 2));
		database.clear();
	}

	public void testInitHyPeerWebDatabase() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		String name = database.getDBName();
		database.clear();
		database.add(new Node(0));
		database.initHyPeerWebDatabase("Test.sqlite");
		File check = new File("Test.sqlite");
		assertTrue(check.exists());
		assertTrue(database.getNode(0) == null);
		database.add(new Node(0));
		SimplifiedNodeDomain sndExpected = new Node(0)
				.constructSimplifiedNodeDomain();
		SimplifiedNodeDomain sndGot = database.getNode(0);
		assertTrue(sndExpected.toString().equals(sndGot.toString()));
		database.clear();
		database.initHyPeerWebDatabase(name);
		database.clear();
	}

	public void testSaveHyPeerWeb() {
		Node[] setOfNodes = { new Node(0), new Node(1), new Node(2) };
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		database.saveHyPeerWeb(setOfNodes);
		for (int i = 0; i < 3; i++) {
			SimplifiedNodeDomain sndExpected = new Node(i)
					.constructSimplifiedNodeDomain();
			SimplifiedNodeDomain sndGot = database.getNode(i);
			assertTrue(sndExpected.toString().equals(sndGot.toString()));
		}
		assertTrue(null == database.getNode(3));
		assertTrue(null == database.getNode(Integer.MAX_VALUE / 2));
		assertTrue(null == database.getNode(-1));
		assertTrue(null == database.getNode(Integer.MIN_VALUE / 2));
		database.clear();
	}

	public void testDelete() {
		Node[] setOfNodes = { new Node(0), new Node(1), new Node(2) };
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		database.saveHyPeerWeb(setOfNodes);
		for (int i = 0; i < 3; i++) {
			SimplifiedNodeDomain sndExpected = new Node(i)
					.constructSimplifiedNodeDomain();
			SimplifiedNodeDomain sndGot = database.getNode(i);
			assertTrue(sndExpected.toString().equals(sndGot.toString()));
		}
		assertTrue(null == database.getNode(3));
		assertTrue(null == database.getNode(Integer.MAX_VALUE / 2));
		assertTrue(null == database.getNode(-1));
		assertTrue(null == database.getNode(Integer.MIN_VALUE / 2));
		database.delete(new Node(0));
		database.delete(new Node(2));
		SimplifiedNodeDomain sndExpected = new Node(1)
				.constructSimplifiedNodeDomain();
		SimplifiedNodeDomain sndGot = database.getNode(1);
		assertTrue(sndExpected.toString().equals(sndGot.toString()));
		assertTrue(null == database.getNode(0));
		assertTrue(null == database.getNode(2));
		assertTrue(null == database.getNode(3));
		assertTrue(null == database.getNode(Integer.MAX_VALUE / 2));
		assertTrue(null == database.getNode(-1));
		assertTrue(null == database.getNode(Integer.MIN_VALUE / 2));
		database.clear();
	}

	public void testAddQueue() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		database.addQueue(new Node(0));
		database.addQueue(new Node(1));
		database.addQueue(new Node(2));
		database.addQueue(new Node(3));
		database.commit();
		for (int i = 0; i <= 3; i++) {
			SimplifiedNodeDomain sndExpected = new Node(i)
					.constructSimplifiedNodeDomain();
			SimplifiedNodeDomain sndGot = database.getNode(i);
			assertTrue(sndExpected.toString().equals(sndGot.toString()));
		}
		assertTrue(null == database.getNode(4));
		assertTrue(null == database.getNode(Integer.MAX_VALUE / 2));
		assertTrue(null == database.getNode(-1));
		assertTrue(null == database.getNode(Integer.MIN_VALUE / 2));
		database.clear();
	}

	public void testAddNeighborAndGetNeighbors() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		Node[] setOfNodes = { new Node(0), new Node(1), new Node(2) };
		database.saveHyPeerWeb(setOfNodes);
		setOfNodes[0].addNeighbor(setOfNodes[1]);
		setOfNodes[1].addNeighbor(setOfNodes[2]);
		setOfNodes[2].addNeighbor(setOfNodes[0]);
		database.addNeighbor(setOfNodes[0], setOfNodes[1], "n");
		database.addNeighbor(setOfNodes[0], setOfNodes[2], "n");
		database.addNeighbor(setOfNodes[1], setOfNodes[0], "sn");
		database.addNeighbor(setOfNodes[1], setOfNodes[2], "sn");
		ArrayList<Node> neighbors = database
				.getNeighborsFromDataBase(setOfNodes[0]);
		for (int i = 0; i < 2; i++) {
			assertTrue(neighbors.get(i).getValue() == i + 1);
		}
		ArrayList<Node> sNeighbors = database
				.getSurrogateNeighborsFromDataBase(setOfNodes[1]);
		assertTrue(sNeighbors.get(0).getValue() == 0);
		assertTrue(sNeighbors.get(1).getValue() == 2);
		ArrayList<Node> isNeighbors = database
				.getInverseSurrogateNeighborsFromDataBase(setOfNodes[0]);
		assertTrue(isNeighbors.get(0).getValue() == 1);
		isNeighbors = database
				.getInverseSurrogateNeighborsFromDataBase(setOfNodes[2]);
		assertTrue(isNeighbors.get(0).getValue() == 1);
		database.clear();
	}

	public void testAddNeighborQueue() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		Node[] setOfNodes = { new Node(0), new Node(1), new Node(2) };
		database.saveHyPeerWeb(setOfNodes);
		setOfNodes[0].addNeighbor(setOfNodes[1]);
		setOfNodes[1].addNeighbor(setOfNodes[2]);
		setOfNodes[2].addNeighbor(setOfNodes[0]);
		database.addNeighborQueue(setOfNodes[0], setOfNodes[1], "n");
		database.addNeighborQueue(setOfNodes[0], setOfNodes[2], "n");
		database.addNeighborQueue(setOfNodes[1], setOfNodes[0], "sn");
		database.addNeighborQueue(setOfNodes[1], setOfNodes[2], "sn");
		database.commit();
		ArrayList<Node> neighbors = database
				.getNeighborsFromDataBase(setOfNodes[0]);
		for (int i = 0; i < 2; i++) {
			assertTrue(neighbors.get(i).getValue() == i + 1);
		}
		ArrayList<Node> sNeighbors = database
				.getSurrogateNeighborsFromDataBase(setOfNodes[1]);
		assertTrue(sNeighbors.get(0).getValue() == 0);
		assertTrue(sNeighbors.get(1).getValue() == 2);
		ArrayList<Node> isNeighbors = database
				.getInverseSurrogateNeighborsFromDataBase(setOfNodes[0]);
		assertTrue(isNeighbors.get(0).getValue() == 1);
		isNeighbors = database
				.getInverseSurrogateNeighborsFromDataBase(setOfNodes[2]);
		assertTrue(isNeighbors.get(0).getValue() == 1);
		database.clear();
	}

	public void testRemoveNeighborQueue() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		Node[] setOfNodes = { new Node(0), new Node(1), new Node(2) };
		database.saveHyPeerWeb(setOfNodes);
		setOfNodes[0].addNeighbor(setOfNodes[1]);
		setOfNodes[1].addNeighbor(setOfNodes[2]);
		setOfNodes[2].addNeighbor(setOfNodes[0]);
		database.addNeighborQueue(setOfNodes[0], setOfNodes[1], "n");
		database.addNeighborQueue(setOfNodes[0], setOfNodes[2], "n");
		database.addNeighborQueue(setOfNodes[1], setOfNodes[0], "sn");
		database.addNeighborQueue(setOfNodes[1], setOfNodes[2], "sn");
		database.commit();
		ArrayList<Node> neighbors = database
				.getNeighborsFromDataBase(setOfNodes[0]);
		for (int i = 0; i < 2; i++) {
			assertTrue(neighbors.get(i).getValue() == i + 1);
		}
		ArrayList<Node> sNeighbors = database
				.getSurrogateNeighborsFromDataBase(setOfNodes[1]);
		assertTrue(sNeighbors.get(0).getValue() == 0);
		assertTrue(sNeighbors.get(1).getValue() == 2);
		ArrayList<Node> isNeighbors = database
				.getInverseSurrogateNeighborsFromDataBase(setOfNodes[0]);
		assertTrue(isNeighbors.get(0).getValue() == 1);
		isNeighbors = database
				.getInverseSurrogateNeighborsFromDataBase(setOfNodes[2]);
		assertTrue(isNeighbors.get(0).getValue() == 1);
		database.removeNeighborQueue(setOfNodes[0], setOfNodes[1]);
		database.removeNeighborQueue(setOfNodes[1], setOfNodes[0]);
		neighbors = database.getNeighborsFromDataBase(setOfNodes[0]);
		assertTrue(neighbors.get(0).getValue() == 2);
		sNeighbors = database.getSurrogateNeighborsFromDataBase(setOfNodes[1]);
		assertTrue(sNeighbors.get(0).getValue() == 2);
		isNeighbors = database
				.getInverseSurrogateNeighborsFromDataBase(setOfNodes[0]);
		assertTrue(isNeighbors.isEmpty());
		database.clear();
	}

	public void testUpdate() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		Node node0 = new Node(0);
		Node node1 = new Node(1);
		database.addQueue(node0);
		database.addQueue(node1);
		database.commit();
		for (int i = 0; i < 2; i++) {
			SimplifiedNodeDomain sndExpected = new Node(i)
					.constructSimplifiedNodeDomain();
			SimplifiedNodeDomain sndGot = database.getNode(i);
			assertTrue(sndExpected.toString().equals(sndGot.toString()));
		}
		node0.setFold(node1);
		database.update(node0);
		SimplifiedNodeDomain sndExpected = node0
				.constructSimplifiedNodeDomain();
		SimplifiedNodeDomain sndGot = database.getNode(0);
		assertTrue(sndExpected.toString().equals(sndGot.toString()));
		database.clear();
	}

	public void testGetNodesFromDataBase() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		database.addQueue(new Node(0));
		database.addQueue(new Node(1));
		database.addQueue(new Node(2));
		database.addQueue(new Node(3));
		database.commit();
		ArrayList<Node> nodes = database.getNodesFromDataBase();
		for (int i = 0; i <= 3; i++) {
			SimplifiedNodeDomain sndExpected = new Node(i)
					.constructSimplifiedNodeDomain();
			SimplifiedNodeDomain sndGot = nodes.get(i)
					.constructSimplifiedNodeDomain();
			assertTrue(sndExpected.toString().equals(sndGot.toString()));
		}
		assertTrue(null == database.getNode(4));
		assertTrue(null == database.getNode(Integer.MAX_VALUE / 2));
		assertTrue(null == database.getNode(-1));
		assertTrue(null == database.getNode(Integer.MIN_VALUE / 2));
		database.clear();
	}

	public void testGetNodesFromDataBaseString() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		String name = database.getDBName();
		database.clear();
		database.addQueue(new Node(0));
		database.addQueue(new Node(1));
		database.addQueue(new Node(2));
		database.addQueue(new Node(3));
		database.commit();
		ArrayList<Node> nodes = database.getNodesFromDataBase();
		for (int i = 0; i <= 3; i++) {
			SimplifiedNodeDomain sndExpected = new Node(i)
					.constructSimplifiedNodeDomain();
			SimplifiedNodeDomain sndGot = nodes.get(i)
					.constructSimplifiedNodeDomain();
			assertTrue(sndExpected.toString().equals(sndGot.toString()));
		}
		assertTrue(null == database.getNode(4));
		assertTrue(null == database.getNode(Integer.MAX_VALUE / 2));
		assertTrue(null == database.getNode(-1));
		assertTrue(null == database.getNode(Integer.MIN_VALUE / 2));
		File source = new File(name);
		File target = new File("testGet.sqlite");
		try {
			Files.copy(source.toPath(), target.toPath(), REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
		nodes = database.getNodesFromDataBase("testGet.sqlite");
		for (int i = 0; i <= 3; i++) {
			SimplifiedNodeDomain sndExpected = new Node(i)
					.constructSimplifiedNodeDomain();
			SimplifiedNodeDomain sndGot = nodes.get(i)
					.constructSimplifiedNodeDomain();
			assertTrue(sndExpected.toString().equals(sndGot.toString()));
		}
		database.clear();
	}

	public void testClear() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		database.add(new Node(0));
		SimplifiedNodeDomain sndExpected = new Node(0)
				.constructSimplifiedNodeDomain();
		SimplifiedNodeDomain sndGot = database.getNode(0);
		assertTrue(sndExpected.toString().equals(sndGot.toString()));
		assertTrue(null == database.getNode(1));
		assertTrue(null == database.getNode(Integer.MAX_VALUE / 2));
		assertTrue(null == database.getNode(-1));
		assertTrue(null == database.getNode(Integer.MIN_VALUE / 2));
		database.clear();
		assertTrue(null == database.getNode(0));
	}

	public void testDeleteReplace() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		Node node0 = new Node(0);
		Node node1 = new Node(1);
		Contents content = new Contents();
		content.set("key", "value");
		node0.setMyContents(content);
		database.add(node0);
		database.add(node1);
		database.deleteReplace(node1, 0);
		ArrayList<Node> nodes = database.getNodesFromDataBase();
		assertTrue(nodes.get(0).getValue() == 1);
		assertTrue(nodes.get(0).getContents().get("key").equals("value"));
		database.clear();
	}

	// Code Coverage Extra
	public void testNodeProxy() {
		HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();
		database.clear();
		Node proxy = new NodeProxy(new GlobalObjectId("localhost",
				new PortNumber(919191), new LocalObjectId(123)));
		database.add(proxy);
		database.getNodesFromDataBase();
		database.clear();
		// phase2.testing.Exp.main(null);
	}
}
