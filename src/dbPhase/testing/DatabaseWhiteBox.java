package dbPhase.testing;

import java.util.ArrayList;

import junit.framework.TestCase;
import phase2.testing.ExpectedResult;
import dbPhase.hypeerweb.HyPeerWeb;
import dbPhase.hypeerweb.HyPeerWebDatabase;
import dbPhase.hypeerweb.Node;
import dbPhase.hypeerweb.SimplifiedNodeDomain;

public class DatabaseWhiteBox extends TestCase {

	public DatabaseWhiteBox(String name) {
		super(name);
	}

	// Loop Testin'!
	public void testAdd() {
		HyPeerWeb hWeb = HyPeerWeb.getSingleton();
		HyPeerWebDatabase database = hWeb.getHyPeerWebDatabase();
		hWeb.addToHyPeerWeb(new Node(0));
		ArrayList<Node> nodes = database.getNodesFromDataBase();
		assertTrue(nodes.size() == 1);
		SimplifiedNodeDomain sndExpected = new Node(0)
				.constructSimplifiedNodeDomain();
		SimplifiedNodeDomain sndGot = database.getNode(0);
		assertTrue(sndExpected.toString().equals(sndGot.toString()));
		hWeb.addToHyPeerWeb(new Node(0));
		sndGot = database.getNode(0);
		ExpectedResult exp = new ExpectedResult(2, 0);
		assertTrue(sndGot.toString().equals(exp.toString()));
		database.clear();
	}

	// Internal Boundary Testing
	public void testUpdate() {
		fail("Not yet implemented"); // TODO
	}

	// Relational Testing
	public void testDeleteReplace() {
		fail("Not yet implemented"); // TODO
	}

}
