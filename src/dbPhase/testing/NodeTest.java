package dbPhase.testing;

import junit.framework.TestCase;
import dbPhase.hypeerweb.Node;

public class NodeTest extends TestCase {

	private Node test = new Node(0);
	@SuppressWarnings("unused")
	private Node test2 = new Node(0, 4);

	public NodeTest(String name) {
		super(name);
	}

	public void testDownPointer() {
		Node downPointer = new Node(1);
		new Node(1);

		test.addDownPointer(downPointer);
		// TEST TO SEE IF IT ADDS NEW DOWNPOINTERS
		assertEquals(test.getDownPointers().size(), 1);
		// TEST TO SEE IF IT RECOGNIZES THEM
		assertEquals(test.getDownPointers().get(0), downPointer);

		test.removeDownPointer(downPointer);
		// TEST TO SEE IF IT IS REMOVED
		assertEquals(test.getDownPointers().size(), 0);
	}

	public void testUpPointer() {
		Node upPointer = new Node(1);
		new Node(1);

		test.addUpPointer(upPointer);
		// TEST TO SEE IF IT ADDS NEW UpPOINTERS
		assertEquals(test.getUpPointers().size(), 1);
		// TEST TO SEE IF IT RECOGNIZES THEM
		assertEquals(test.getUpPointers().get(0), upPointer);

		test.removeUpPointer(upPointer);
		// TEST TO SEE IF IT IS REMOVED
		assertEquals(test.getUpPointers().size(), 0);
	}

	public void testNeighbor() {
		Node neightbor = new Node(1);
		new Node(1);

		test.addNeighbor(neightbor);
		// TEST TO SEE IF IT ADDS NEW UpPOINTERS
		assertEquals(test.getNeighbors().size(), 1);

		// TEST TO SEE IF IT RECOGNIZES THEM
		assertEquals(test.getNeighbors().get(0), neightbor);

		test.removeNeighbor(neightbor);
		// TEST TO SEE IF IT IS REMOVED
		assertEquals(test.getNeighbors().size(), 0);
	}

}
