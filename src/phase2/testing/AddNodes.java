package phase2.testing;

import junit.framework.TestCase;
import phase2.states.HypercubeCapState;
import phase2.states.StandardNodeState;
import phase2.states.TerminalNodeState;
import phase2.states.UpPointingNodeState;
import dbPhase.hypeerweb.HyPeerWeb;
import dbPhase.hypeerweb.Node;

public class AddNodes extends TestCase {

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	@SuppressWarnings("unused")
	private Node test = new Node(0);
	@SuppressWarnings("unused")
	private Node test2 = new Node(0, 4);

	public void testAddOneToHyPeerWeb() {
		HyPeerWeb h = HyPeerWeb.getSingleton();

		Node n1 = new Node(0);

		h.addToHyPeerWeb(n1);

		// TEST TO SEE IF NEW NODE IS IN HYPEERWEB
		assertEquals(h.getNode(0).getValue(), n1.getValue());

		assertEquals(h.getNode(0).getFold().getValue(), n1.getValue());

		assertEquals(h.getNode(0).getSFold().getValue(),
				Node.NULL_NODE.getValue());

		assertEquals(h.getNode(0).getISFold().getValue(),
				Node.NULL_NODE.getValue());

		assertEquals(h.getNode(0).getState().getNodeState(),
				new HypercubeCapState().getNodeState());
		h.clear();
	}

	public void testAddOneToNodes() {

		Node n0 = new Node(0);
		Node n1 = new Node(1);

		n0.addToHyPeerWeb(n1);

		// TEST TO SEE IF IT ADDED THE NODE CORRECTELY

		assertEquals(n0.getState().getNodeState(),
				new StandardNodeState().getNodeState());
		assertEquals(n0.getNeighbors().size(), 1);
		assertEquals(n0.getNeighbors().get(0).getValue(), n1.getValue());
		assertEquals(n0.getFold().getValue(), n1.getValue());
		assertEquals(n0.getSFold().getValue(), Node.NULL_NODE.getValue());
		assertEquals(n0.getISFold().getValue(), Node.NULL_NODE.getValue());

		assertEquals(n1.getState().getNodeState(),
				new HypercubeCapState().getNodeState());
		assertEquals(n1.getNeighbors().size(), 1);
		assertEquals(n1.getNeighbors().get(0).getValue(), n0.getValue());
		assertEquals(n1.getFold().getValue(), n0.getValue());
		assertEquals(n1.getSFold().getValue(), Node.NULL_NODE.getValue());
		assertEquals(n1.getISFold().getValue(), Node.NULL_NODE.getValue());

	}

	public void testAddTwoToNodes() {

		Node n0 = new Node(0);
		Node n1 = new Node(1);
		Node n2 = new Node(2);

		n0.addToHyPeerWeb(n1);
		n0.addToHyPeerWeb(n2);

		// TEST TO SEE IF IF IT ADDED BOTH NODES CORRECTLY

		assertEquals(n0.getState().getNodeState(),
				new StandardNodeState().getNodeState());
		assertEquals(n0.getNeighbors().size(), 2);
		assertEquals(n0.getFold().getValue(), Node.NULL_NODE.getValue());
		assertEquals(n0.getSFold().getValue(), n1.getValue());
		assertEquals(n0.getISFold().getValue(), Node.NULL_NODE.getValue());

		assertEquals(n1.getState().getNodeState(),
				new UpPointingNodeState().getNodeState());
		assertEquals(n2.getNeighbors().size(), 1);
		assertEquals(n1.getFold().getValue(), n2.getValue());
		assertEquals(n1.getSFold().getValue(), Node.NULL_NODE.getValue());
		assertEquals(n1.getISFold().getValue(), n0.getValue());

		assertEquals(n2.getState().getNodeState(),
				new TerminalNodeState().getNodeState());
		assertEquals(n2.getNeighbors().size(), 1);
		assertEquals(n2.getFold().getValue(), n1.getValue());
		assertEquals(n2.getSFold().getValue(), Node.NULL_NODE.getValue());
		assertEquals(n2.getISFold().getValue(), Node.NULL_NODE.getValue());

	}

	public void testAddThreeToNodes() {

		Node n0 = new Node(0);
		Node n1 = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);

		n0.addToHyPeerWeb(n1);
		n0.addToHyPeerWeb(n2);
		n0.addToHyPeerWeb(n3);

		// TEST TO SEE IF IF IT ADDED BOTH NODES CORRECTLY

		assertEquals(n0.getState().getNodeState(),
				new StandardNodeState().getNodeState());
		assertEquals(n0.getNeighbors().size(), 2);
		assertEquals(n0.getFold().getValue(), n3.getValue());
		assertEquals(n0.getSFold().getValue(), Node.NULL_NODE.getValue());
		assertEquals(n0.getISFold().getValue(), Node.NULL_NODE.getValue());

		assertEquals(n1.getState().getNodeState(),
				new StandardNodeState().getNodeState());
		assertEquals(n1.getNeighbors().size(), 2);
		assertEquals(n1.getFold().getValue(), n2.getValue());
		assertEquals(n1.getSFold().getValue(), Node.NULL_NODE.getValue());
		assertEquals(n1.getISFold().getValue(), Node.NULL_NODE.getValue());

		assertEquals(n2.getState().getNodeState(),
				new StandardNodeState().getNodeState());
		assertEquals(n1.getNeighbors().size(), 2);
		assertEquals(n2.getFold().getValue(), n1.getValue());
		assertEquals(n2.getSFold().getValue(), Node.NULL_NODE.getValue());
		assertEquals(n2.getISFold().getValue(), Node.NULL_NODE.getValue());

		assertEquals(n3.getState().getNodeState(),
				new HypercubeCapState().getNodeState());
		assertEquals(n3.getNeighbors().size(), 2);
		assertEquals(n3.getFold().getValue(), n0.getValue());
		assertEquals(n3.getSFold().getValue(), Node.NULL_NODE.getValue());
		assertEquals(n3.getISFold().getValue(), Node.NULL_NODE.getValue());

	}

}
