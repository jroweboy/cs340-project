package phase2.states;

import java.io.Serializable;

public class StandardNodeState extends NodeState implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4369151319562053572L;
	public static final int STATE_ID = 0;

	@Override
	public int getNodeState() {
		return STATE_ID;
	}

	@Override
	public String toString() {
		return "StandardNodeState";
	}

}
