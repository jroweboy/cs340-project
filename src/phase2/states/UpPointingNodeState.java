package phase2.states;

import java.io.Serializable;

public class UpPointingNodeState extends NodeState implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1999345010224541757L;
	public static final int STATE_ID = 1;

	@Override
	public int getNodeState() {
		return STATE_ID;
	}

	@Override
	public String toString() {
		return "UpPointingNodeState";
	}
}
