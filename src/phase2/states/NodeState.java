package phase2.states;

import java.io.Serializable;

import dbPhase.hypeerweb.Node;

/**
 * The abstract class of the State of a node in the HyPeerWeb.<br>
 * <br>
 * 
 * <pre>
 * <b>Domain:</b>
 * 
 * 
 * <b>Invariant:</b>
 * 
 * 
 </pre>
 * 
 * @author Thomas Holladay
 */
public abstract class NodeState implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8324351543993660373L;

	/**
	 * takes the parent and child node and reconnects their folds correctly;
	 * 
	 * @param parent
	 *            the node that has a child
	 * @param child
	 *            the node that was recently added and will have a parent
	 * @pre <i>child.fold = null</i>
	 * @post child.fold does not equal null
	 */
	public void setFolds(Node parent, Node child) {

		if (parent.getISFold() != null) {
			Node isFold = parent.getISFold();

			isFold.setFold(child);
			isFold.setSurrogateFold(null);

			parent.setInverseSurrogateFold(null);

			child.setFold(isFold);
		} else {
			Node fold = parent.getFold();

			fold.setFold(child);
			child.setFold(fold);
			parent.setSurrogateFold(fold);
			parent.setFold(null);

		}

	}

	/**
	 * returns the defalut node state;
	 * 
	 * @pre <i>None</i>
	 * @post result=-1
	 */
	public int getNodeState() {
		return -1;
	}

	/**
	 * returns correct Node state of the based on an integer number
	 * 
	 * @param s
	 *            an integer that will be used to determine what NodeState
	 *            should be returned
	 * @pre <i>None</i>
	 * @post result equals one of the 5 node states or null
	 */
	public static NodeState getNodeState(int s) {
		switch (s) {
		case 0:
			return (new StandardNodeState());
		case 1:
			return (new UpPointingNodeState());
		case 2:
			return (new DownPointingNodeState());
		case 3:
			return (new HypercubeCapState());
		case 4:
			return (new TerminalNodeState());
		default:
			return null;
		}
	}
}
