package phase2.states;

import java.io.Serializable;

/**
 * @author Thomas Holladay
 * 
 */
public class DownPointingNodeState extends NodeState implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2489452837381322364L;
	public static final int STATE_ID = 2;

	@Override
	public int getNodeState() {
		return STATE_ID;
	}

	@Override
	public String toString() {
		return "DownPointingNodeState";
	}
}