package phase2.states;

import java.io.Serializable;

public class TerminalNodeState extends NodeState implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -543474992198610981L;
	public static final int STATE_ID = 4;

	@Override
	public int getNodeState() {
		return STATE_ID;
	}

	@Override
	public String toString() {
		return "TerminalNodeState";
	}
}
