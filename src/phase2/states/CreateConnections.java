package phase2.states;

import java.util.ArrayList;

import dbPhase.hypeerweb.HyPeerWebDatabase;
import dbPhase.hypeerweb.Node;

/**
 * 
 * @author Thomas Holladay
 * 
 */
public class CreateConnections {

	private Node parent;
	private Node child;
	@SuppressWarnings("unused")
	private HyPeerWebDatabase database = HyPeerWebDatabase.getSingleton();

	/**
	 * @author Thomas Holladay
	 * @param p
	 * @param c
	 */
	public void makeConnections(Node p, Node c) {
		parent = p;
		child = c;

		parent.addNeighbor(child);
		child.addNeighbor(parent);
		fixFolds();
		fixNeighbors();
	}

	public void fixConnectionsAfterDeletion() {

	}

	/**
	 * @author Thomas Holladay
	 */
	public void fixFolds() {
		if (child.getWebId().getValue() == 1) {
			parent.setFold(child);
			child.setFold(parent);
		} else if (!parent.getISFold().equals(Node.NULL_NODE)) {
			Node isFold = parent.getISFold();

			isFold.setFold(child);
			isFold.setSurrogateFold(Node.NULL_NODE);

			parent.setInverseSurrogateFold(Node.NULL_NODE);
			child.setFold(isFold);
		} else {
			Node fold = parent.getFold();

			fold.setFold(child);
			child.setFold(fold);
			parent.setSurrogateFold(fold);
			parent.setFold(Node.NULL_NODE);

			fold.setInverseSurrogateFold(parent);
		}
	}

	/**
	 * @author Robert Hickman
	 */
	public void fixNeighbors() {
		ArrayList<Node> parentUpPointers = new ArrayList<Node>(
				parent.getUpPointers());
		for (Node neighbor : parentUpPointers) {
			child.addNeighbor(neighbor);
			neighbor.removeDownPointer(this.parent);
			neighbor.addNeighbor(child);
			parent.removeUpPointer(neighbor);
		}
		for (Node neighbor : parent.getNeighbors()) {
			if (neighbor.getWebId().getValue() == child.getWebId().getValue()) {
				continue;
			}
			if (neighbor.getWebId().getValue() > parent.getWebId().getValue()) {
				child.addDownPointer(neighbor);
				neighbor.addUpPointer(child);
			}
		}
	}
}
