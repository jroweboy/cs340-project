package phase2.states;

import java.io.Serializable;

public class HypercubeCapState extends NodeState implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1660719786034270625L;
	public static final int STATE_ID = 3;

	@Override
	public int getNodeState() {
		return STATE_ID;
	}

	@Override
	public String toString() {
		return "HypercubeCapState";
	}
}
