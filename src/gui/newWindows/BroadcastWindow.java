package gui.newWindows;

import gui.Main.GUI;

public class BroadcastWindow extends PopUpWindow {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1680222129453580614L;

	public BroadcastWindow(GUI main, String title) {
		super(main, title);
	}

	@Override
	protected void addPanel() {
		panel = new BroadcastWindowPanel(main);
		this.add(panel);
	}
}
