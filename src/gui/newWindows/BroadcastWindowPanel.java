package gui.newWindows;

import gui.Broadcaster;
import gui.Main.GUI;
import gui.Main.HyPeerWeb;
import gui.mapper.NodeListing;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import phase4.classes.Parameters;
import dbPhase.hypeerweb.WebId;

public class BroadcastWindowPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -271964212122326922L;
	protected GUI main;
	protected JLabel startingNodeLabel;
	protected JLabel messageBoxLabel;
	protected JTextField startingNode;
	protected JTextField messageBox;
	protected JButton broadcastButton;

	public BroadcastWindowPanel(GUI main) {
		// super(new GridBagLayout());
		super(new GridLayout(3, 1));
		this.main = main;

		startingNodeLabel = new JLabel("Starting Node");
		messageBoxLabel = new JLabel("Message");

		startingNode = new JTextField(3);
		messageBox = new JTextField(20);

		// Build the send button
		broadcastButton = new JButton("Broadcast Message");
		broadcastButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				broadcastButtonPressed();
			}
		});

		JPanel startingEndingNodePanel = new JPanel();
		startingEndingNodePanel.add(startingNodeLabel);
		startingEndingNodePanel.add(startingNode);
		this.add(startingEndingNodePanel);

		JPanel messageNodePanel = new JPanel();
		messageNodePanel.add(messageBoxLabel);
		messageNodePanel.add(messageBox);
		this.add(messageNodePanel);

		this.add(broadcastButton);

	}

	@SuppressWarnings("unused")
	private void setBroadcastWindowToNull() {
		main.getHyPeerWebDebugger().getStandardCommands()
				.setBroadcastWindowToNull();
	}

	private void broadcastButtonPressed() {
		// TODO Phase 5 -- starting at the indicated node, broadcast the
		// provided message to all nodes in the HyPeerWeb.
		// I. Get the text in the "startingNode" component and convert it to an
		// integer.
		// A. If the indicated start node is empty, or does not contain an
		// integer, or does not identify an
		// existing node in the HyPeerWeb, post an error message in the
		// "debugStatus" component of the GUI.
		// B. Otherwise, get the message from the "messageBox" component and
		// broadcast it to all nodes in the HyPeerWeb,
		// starting at the indicated start node, using the Broadcaster visitor.
		int start;
		try {
			start = Integer.parseInt(startingNode.getText());
		} catch (NumberFormatException e) {
			main.printToTracePanel("Error: Invalid input. Cannot broadcast.\n");
			return;
		}
		HyPeerWeb hypeerweb = main.getHyPeerWeb();
		NodeListing listing = main.getHyPeerWebDebugger().getMapper()
				.getNodeListing();
		listing.getSelectedIndex();
		if (!hypeerweb.contains(start)) {
			main.printToTracePanel("Error: Cannot broadcast from a non existant node\n");
			return;
		}
		Parameters parameters = Broadcaster.createInitialParameters(messageBox
				.getText());
		hypeerweb.getNode(start).find(new WebId(0))
				.accept(new Broadcaster(GUI.getProxy()), parameters);
		JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(this);
		topFrame.dispose();
		// int size = listing.listSize();
		// if (size == 1){
		// main.printToTracePanel("Error: Thou shalt not remove the final node\n");
		// } else {
		// //Node n = new Node(0);
		// int selected = listing.getSelectedIndex();
		// //HyPeerWeb hypeerweb = main.getHyPeerWeb();
		// if (!hypeerweb.contains(selected)){
		// main.printToTracePanel("Error: Cannot delete a non existent node");
		// } else {
		// //hypeerweb.addToHyPeerWeb(n, hypeerweb.getNode(selected));
		// hypeerweb.removeNode(hypeerweb.getNode(selected));
		// listing.decreaseListSize();
		// }
		// }

	}
}
