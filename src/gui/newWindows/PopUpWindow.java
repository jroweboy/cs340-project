package gui.newWindows;

import gui.Main.GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;

public abstract class PopUpWindow extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2497086853537145524L;
	protected GUI main;
	protected JPanel panel;

	public PopUpWindow(GUI main, String title) {
		super(title);
		this.main = main;
		init();
	}

	public void init() {
		// this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());
		this.setPreferredSize(new Dimension(325, 115));

		addPanel();

		this.pack();
		this.setVisible(true);
	}

	public JPanel getPanel() {
		return panel;
	}

	protected abstract void addPanel();

}
