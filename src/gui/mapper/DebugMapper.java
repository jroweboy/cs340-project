package gui.mapper;

import gui.Main.GUI;

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class DebugMapper extends JPanel {

	// private JTabbedPane content;

	/**
	 * 
	 */
	private static final long serialVersionUID = -8345950473070925528L;
	private NodeListing mainTab;

	public DebugMapper(GUI main) {
		this.setLayout(new BorderLayout());
		this.setBorder(BorderFactory
				.createTitledBorder("List Of Nodes In HyPeerWeb"));

		// content = new JTabbedPane();

		mainTab = new NodeListing(main);

		// content.addTab("Node Listing",mainTab);

		// this.add(content);
		this.add(mainTab);
	}

	public NodeListing getNodeListing() {
		return mainTab;
	}

}
