package gui.menus;

import gui.Main.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * Represents the file Menu
 * 
 * @author msmith52
 * 
 */
public class DebugFileMenu extends JMenu {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2096928999346729985L;

	/** Root the GUI **/
	private GUI main;

	/** Debug session menu */
	private DebugSessionMenu sessionMenu;

	/** Exit menu item */
	private JMenuItem exit;

	/**
	 * Creates a Debug File Menu object
	 * 
	 * @param main
	 */
	DebugFileMenu(GUI main) {
		this.main = main;

		init();
	}

	/**
	 * Initializes GUI components
	 */
	public void init() {
		this.setText("File");

		sessionMenu = new DebugSessionMenu(main);
		this.add(sessionMenu);

		exit = new JMenuItem("Exit");
		exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				destroy();
				System.out
						.println("BROKEN!! PLEASE FIX ME OR DIE TRYING. IMPLEMENT THE HYPEERWEBCLOSE METHOD PLOX");
				// main.getHyPeerWeb().close();
				System.exit(0);
			}

		});

		this.add(exit);
	}

	/**
	 * Called before the program exits
	 */
	public void destroy() {
		// TODO Phase 5 -- clean up before exiting the program
		// Not sure what I need to do here
		System.out
				.println("BROKEN!! PLEASE FIX ME OR DIE TRYING. IMPLEMENT THE HYPEERWEBCLOSE METHOD PLOX");
		// main.getHyPeerWeb().close();
	}
}
