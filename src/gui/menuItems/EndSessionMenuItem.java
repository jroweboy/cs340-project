package gui.menuItems;

import gui.Main.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

/**
 * Represents the end session menu item presented in the Help menu
 * 
 * @author Matthew Smith
 * 
 */
public class EndSessionMenuItem extends JMenuItem implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2674077473864463908L;
	GUI main;

	/**
	 * Creates an End session menu Item
	 * 
	 * @param main
	 */
	public EndSessionMenuItem(GUI main) {
		this.main = main;

		init();
	}

	/**
	 * initializes the GUI components
	 */
	public void init() {
		this.setText("End");

		this.addActionListener(this);
	}

	/**
	 * Action when menu item is pressed
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

		// TODO Phase 6 -- provide functionality for terminating an existing
		// HyPeerWeb

	}

}
