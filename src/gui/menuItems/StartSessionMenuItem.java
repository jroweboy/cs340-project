package gui.menuItems;

import gui.Main.GUI;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import phase6.GlobalObjectId;
import phase6.LocalObjectId;
import phase6.PortNumber;
import phase6.Shell;

/**
 * Represents the start session menu item presented in the Help menu
 * 
 * @author Matthew Smith
 * 
 */
public class StartSessionMenuItem extends JMenuItem implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5903379005414985054L;
	GUI main;

	/**
	 * Creates a Start Session menu Item
	 * 
	 * @param main
	 */
	public StartSessionMenuItem(GUI main) {
		this.main = main;

		init();
	}

	/**
	 * initializes the GUI components
	 */
	public void init() {
		this.setText("New");

		this.addActionListener(this);
	}

	/**
	 * Action when menu item is pressed
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JTextField field1 = new JTextField("localhost");
		JTextField field2 = new JTextField("50515");
		// JTextField field3 = new JTextField("0");
		JPanel panel = new JPanel(new GridLayout(0, 1));
		panel.add(new JLabel("Create a hypeerweb running on the following"));
		panel.add(new JLabel("IP Address:"));
		panel.add(field1);
		panel.add(new JLabel("Port:"));
		panel.add(field2);
		// panel.add(new JLabel("Local:"));
		// panel.add(field3);
		int result = JOptionPane.showConfirmDialog(null, panel, "Create",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (result == JOptionPane.OK_OPTION) {
			String who = field1.getText().equals("localhost") ? GlobalObjectId
					.getMyIp() : field1.getText();
			PortNumber connect;
			try {
				connect = new PortNumber(Integer.parseInt(field2.getText()));
			} catch (Exception ex) {
				connect = new PortNumber(50515);
			}
			// LocalObjectId id;
			// try {
			// id = new LocalObjectId(Integer.parseInt(field3.getText()));
			// } catch (Exception ex) {
			// id = new LocalObjectId(0);
			// }
			final GlobalObjectId globalObjectId = new GlobalObjectId(who,
					connect, new LocalObjectId(0));
			if (who.equals(GlobalObjectId.getMyIp())) {
				System.out.println("Making one on Localhost");
				Shell.exec("ant main -Dport=" + connect.getValue());
			} else {
				System.out.println("Making one on a remote IP through SSH");
				Shell.exec("sshpass -p test ssh test@" + who
						+ " \"cd cs340-project && ant main -Dport="
						+ connect.getValue() + "\"");
			}
			// ObjectDB.setFileLocation("gui" +
			// globalObjectId.getLocalObjectId()
			// + ".db");
			if (main.getHyPeerWeb().isConnected()) {
				main.getHyPeerWeb().addSegmentToHyPeerWeb(globalObjectId);
			}
			main.getHyPeerWeb().connect(globalObjectId);
			System.out.println("NewId: " + globalObjectId);
		} else {
			System.out.println("Cancelled");
		}

		// my attempt at making it work through the local object db that
		// failed///
		// final GlobalObjectId globalObjectId = new GlobalObjectId(who,
		// connect, id);
		// final GlobalObjectId globalObjectId = new GlobalObjectId();
		// LocalObjectId id = globalObjectId.getLocalObjectId();
		// dbPhase.hypeerweb.HyPeerWeb me =
		// dbPhase.hypeerweb.HyPeerWeb.getSingleton();
		// //System.out.println("Id: " + globalObjectId);
		// ObjectDB.getSingleton().store(id, me);
		// main.printToTracePanel("Hypeerweb running on localid "+ id);
		// System.out.println("Second Dump: ");
		// ObjectDB.getSingleton().dump();
		//
		//
		// HyPeerWeb proxy = main.getHyPeerWeb();
		// proxy.disconnect();
		// //ObjectDB.setFileLocation("gui"+globalObjectId.getLocalObjectId()+".db");
		// proxy.connect(globalObjectId);
		// } else {
		// System.out.println("Cancelled");
		// }
		// Shell.exec("ant -Dport")
	}
}
