package gui.menuItems;

import gui.Main.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

/**
 * Represents the Leave session menu item presented in the Help menu
 * 
 * @author Matthew Smith
 * 
 */
public class LeaveSessionMenuItem extends JMenuItem implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7827792706333994550L;
	GUI main;

	/**
	 * Creates a Leave session menu Item
	 * 
	 * @param main
	 */
	public LeaveSessionMenuItem(GUI main) {
		this.main = main;

		init();
	}

	/**
	 * initializes the GUI components
	 */
	public void init() {
		this.setText("Leave");

		this.addActionListener(this);
	}

	/**
	 * Action when menu item is pressed
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Phase 6 -- provide functionality for leaving a session but no
		// terminate the session

	}

}
