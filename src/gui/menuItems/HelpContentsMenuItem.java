package gui.menuItems;

import gui.Main.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 * Represents the Help menu item presented in the Help menu
 * 
 * @author Matthew Smith
 * 
 */
public class HelpContentsMenuItem extends JMenuItem implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2298539034570748767L;
	GUI main;

	/**
	 * Creates a Help menu Item
	 * 
	 * @param main
	 */
	public HelpContentsMenuItem(GUI main) {
		this.main = main;

		init();
	}

	/**
	 * initializes the GUI components
	 */
	public void init() {
		this.setText("Contents");

		this.addActionListener(this);
	}

	/**
	 * Action when menu item is pressed
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		JOptionPane
				.showMessageDialog(main, "You do NOT need to implement this");
	}

}
