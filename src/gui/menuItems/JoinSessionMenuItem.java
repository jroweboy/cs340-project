package gui.menuItems;

import gui.Main.GUI;
import gui.mapper.NodeListing;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import phase6.GlobalObjectId;
import phase6.LocalObjectId;
import phase6.ObjectDB;
import phase6.PortNumber;

/**
 * Represents the Join session menu item presented in the Help menu
 * 
 * @author Matthew Smith
 * 
 */
public class JoinSessionMenuItem extends JMenuItem implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4764916074181454904L;
	GUI main;

	/**
	 * Creates an Join session menu Item
	 * 
	 * @param main
	 */
	public JoinSessionMenuItem(GUI main) {
		this.main = main;

		init();
	}

	/**
	 * initializes the GUI components
	 */
	public void init() {
		this.setText("Join");

		this.addActionListener(this);
	}

	/**
	 * Action when menu item is pressed
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Phase 6 -- provide functionality for joining an existing
		// HyPeerWebString[] items = {"One", "Two", "Three", "Four", "Five"};
		JTextField field1 = new JTextField("localhost");
		JTextField field2 = new JTextField("50505");
		JTextField field3 = new JTextField("0");
		JPanel panel = new JPanel(new GridLayout(0, 1));
		panel.add(new JLabel("IP Address:"));
		panel.add(field1);
		panel.add(new JLabel("Port:"));
		panel.add(field2);
		panel.add(new JLabel("Local:"));
		panel.add(field3);
		int result = JOptionPane.showConfirmDialog(null, panel, "Join",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
		if (result == JOptionPane.OK_OPTION) {
			String who = field1.getText().equals("localhost") ? GlobalObjectId
					.getMyIp() : field1.getText();
			PortNumber connect;
			try {
				connect = new PortNumber(Integer.parseInt(field2.getText()));
			} catch (Exception ex) {
				connect = new PortNumber(50505);
			}
			LocalObjectId id;
			try {
				id = new LocalObjectId(Integer.parseInt(field3.getText()));
			} catch (Exception ex) {
				id = new LocalObjectId(0);
			}
			final GlobalObjectId globalObjectId = new GlobalObjectId(who,
					connect, id);
			ObjectDB.setFileLocation("gui"
					+ PortNumber.getApplicationsPortNumber() + ".db");
			main.getHyPeerWeb().connect(globalObjectId);
			NodeListing listing = main.getHyPeerWebDebugger().getMapper()
					.getNodeListing();
			int hywebsize = main.getHyPeerWeb().size();
			for (int i = 0; i < hywebsize; i++) {
				listing.increaseListSize();
			}
			System.out.println("Id: " + globalObjectId);
		} else {
			System.out.println("Cancelled");
		}
	}
}
