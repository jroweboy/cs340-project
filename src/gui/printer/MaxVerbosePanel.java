package gui.printer;

import gui.Main.GUI;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JViewport;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;

/**
 * Main debug printing panel that behaves like a text pane and can even display
 * in html the messages printed by Nodes in the hypeerweb
 * 
 * @author Matthew Smith
 * 
 */
public class MaxVerbosePanel extends JPanel implements DPrinter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6911256543588975370L;

	/**
	 * Determines whether or not, when information is printed to the Panel, the
	 * top or bottom of the panel is displayed.
	 */
	private boolean scrollToBottom = false;

	/** The main pane of printer for printing */
	private JTextPane textPane;

	/** Document that acts as the content of the text pane */
	private StyledDocument doc;

	/** container for the text Pane for scrolling */
	private JScrollPane textAreaContainer;

	/** Flag indicating whether this has a clear button. */
	private boolean hasClearButton = false;

	/** The clear button. */
	private JButton clearButton = null;

	/**
	 * Creates a Max Verbose panel
	 * 
	 * @param main
	 */
	public MaxVerbosePanel(GUI main, boolean scrollToBottom,
			boolean hasClearButton) {
		this.scrollToBottom = scrollToBottom;
		this.hasClearButton = hasClearButton;

		init();

		// println("HyPeerWeb debug session ["+getDateTime()+"]");
	}

	/**
	 * Initializes GUI componenets
	 */
	public void init() {
		this.setLayout(new BorderLayout());

		textPane = new JTextPane();
		textPane.setEditable(false);
		doc = textPane.getStyledDocument();

		textAreaContainer = new JScrollPane(textPane);
		textAreaContainer
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		this.add(textAreaContainer, BorderLayout.CENTER);

		if (hasClearButton) {
			// Build the clear button
			clearButton = new JButton("Clear");
			clearButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearButtonPressed();
				}
			});
			clearButton.setMnemonic(KeyEvent.VK_L);

			this.add(clearButton, BorderLayout.SOUTH);
		}
	}

	@Override
	public void print(Object msg) {
		try {
			doc.insertString(doc.getLength(), msg.toString(), null);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}

		scrollToBottom();
	}

	@Override
	public void println() {
		print("\n");
	}

	@Override
	public void println(Object msg) {
		print(msg);
		print("\n");
	}

	public void set(Object msg) {
		textPane.setText(msg.toString());
	}

	/**
	 * Returns a time stamp in the form of a string
	 * 
	 * @return
	 */
	@SuppressWarnings("unused")
	private String getDateTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	/** if auto scrolling is enabled, move window to the base */
	private void scrollToBottom() {
		// if(togglePause.isSelected()){
		if (scrollToBottom) {
			JViewport window = textAreaContainer.getViewport();
			window.setViewPosition(new Point(0, textPane.getHeight()));
		}
	}

	private void clearButtonPressed() {
		textPane.setText("");
	}

}
