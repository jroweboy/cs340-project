package gui.Main;

import java.io.Serializable;

import phase6.Command;
import phase6.GlobalObjectId;
import phase6.MyObserver;
import phase6.PeerCommunicator;

public class GUIProxy implements MyObserver, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2266174614304512645L;
	GlobalObjectId globalObjectId;

	public GUIProxy(GlobalObjectId l) {
		globalObjectId = new GlobalObjectId(l);
	}

	public void printToTracePanel(Object p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "java.lang.Object";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"gui.Main.GUI", "printToTracePanel", parameterTypeNames,
				actualParameters, true);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public void update(String p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "java.lang.String";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"gui.Main.GUI", "update", parameterTypeNames, actualParameters,
				true);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}
}
