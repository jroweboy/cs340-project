package gui.Main;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;

import phase6.Command;
import phase6.GlobalObjectId;
import phase6.PeerCommunicator;
import dbPhase.hypeerweb.HyPeerWebDatabase;
import dbPhase.hypeerweb.Node;

public class HyPeerWeb extends Observable implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3990958496642374851L;
	private GlobalObjectId globalObjectId = null;

	public HyPeerWeb() {

	}

	public HyPeerWeb(GlobalObjectId goid) {
		globalObjectId = goid;
	}

	public boolean isConnected() {
		return this.globalObjectId != null;
	}

	public void disconnect() {
		this.globalObjectId = null;
	}

	public void connect(GlobalObjectId i) {
		this.globalObjectId = i;
		GUIProxy p = GUI.getProxy();
		this.addObserver(p);
	}

	public void setGlobalObjectId(GlobalObjectId goid) {
		globalObjectId = goid;
	}

	public GlobalObjectId getGlobalObjectId() {
		return globalObjectId;
	}

	public void clear() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "clear", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	public boolean contains(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "contains", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Boolean) result;
	}
	
	public boolean contains(int p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "int";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "contains", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Boolean) result;
	}

	public int size() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "size", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		try {
			((Exception) result).printStackTrace();
		} catch (Exception e) {
		}
		return (Integer) result;
	}

	/*
	 * public static HyPeerWeb getSingleton() { String[] parameterTypeNames =
	 * new String[0]; Object[] actualParameters = new Object[0]; Command command
	 * = new Command(globalObjectId.getLocalObjectId(), "HyPeerWeb",
	 * "getSingleton", parameterTypeNames, actualParameters, true); Object
	 * result = PeerCommunicator.getSingleton().sendSynchronous( globalObjectId,
	 * command); return (HyPeerWeb) result; }
	 */

	public void addToHyPeerWeb(Node p0, Node p1) {
		String[] parameterTypeNames = new String[2];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		parameterTypeNames[1] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[2];
		actualParameters[0] = p0;
		actualParameters[1] = p1;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "addToHyPeerWeb",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	public void addToHyPeerWeb(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "addToHyPeerWeb",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	public HyPeerWebDatabase getHyPeerWebDatabase() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "getHyPeerWebDatabase",
				parameterTypeNames, actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (HyPeerWebDatabase) result;
	}

	public Node getNode(int p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "int";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "getNode", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		try {
			((Exception) result).printStackTrace();
		} catch (Exception e) {
		}
		return (Node) result;
	}

	public void reload(java.lang.String p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "java.lang.String";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "reload", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	//
	// public void addObserver(java.util.Observer o) {
	// String[] parameterTypeNames = new String[1];
	// parameterTypeNames[0] = "java.util.Observer";
	// Object[] actualParameters = new Object[1];
	// actualParameters[0] = o;
	// Command command = new Command(globalObjectId.getLocalObjectId(),
	// "java.util.Observable", "addObserver", parameterTypeNames,
	// actualParameters, false);
	// PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
	// command);
	// }

	public void reload() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "reload", parameterTypeNames,
				actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	public void removeNode(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "removeNode",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	public void saveToDatabase() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "saveToDatabase",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	@Override
	public boolean equals(java.lang.Object p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "java.lang.Object";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"java.lang.Object", "equals", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Boolean) result;
	}

	@Override
	public java.lang.String toString() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"java.lang.Object", "toString", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (java.lang.String) result;
	}

	@Override
	public int hashCode() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"java.lang.Object", "hashCode", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (Integer) result;
	}

	public void addSegment(GlobalObjectId p0) {
		// TODO Auto-generated method stub
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "phase6.GlobalObjectId";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "addSegment",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	public void addSegmentToHyPeerWeb(GlobalObjectId p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "phase6.GlobalObjectId";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "addSegmentToHyPeerWeb",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	public void notifyOnce(String p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "java.lang.String";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "notifyOnce",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	public void addObserver(Object p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "phase6.MyObserver";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "addObserver",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	public void notifiedRemove(Object p0, Object p1) {
		String[] parameterTypeNames = new String[2];
		parameterTypeNames[0] = "java.lang.Object";
		parameterTypeNames[1] = "java.lang.Object";
		Object[] actualParameters = new Object[2];
		actualParameters[0] = p0;
		actualParameters[1] = p1;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "notifiedRemove",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}

	public void notifiedAdd(Object p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "java.lang.Object";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "notifiedAdd",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Node> getMyNodes() {
		String[] parameterTypeNames = new String[0];
		Object[] actualParameters = new Object[0];
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.Node", "getMyNodes", parameterTypeNames,
				actualParameters, true);
		Object result = PeerCommunicator.getSingleton().sendSynchronous(
				globalObjectId, command);
		return (ArrayList<dbPhase.hypeerweb.Node>) result;
	}

	public void setNodeZero(Node p0) {
		String[] parameterTypeNames = new String[1];
		parameterTypeNames[0] = "dbPhase.hypeerweb.Node";
		Object[] actualParameters = new Object[1];
		actualParameters[0] = p0;
		Command command = new Command(globalObjectId.getLocalObjectId(),
				"dbPhase.hypeerweb.HyPeerWeb", "setNodeZero",
				parameterTypeNames, actualParameters, false);
		PeerCommunicator.getSingleton().sendASynchronous(globalObjectId,
				command);		
	}

}