package gui.Main;

import gui.mapper.NodeListing;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import phase6.GlobalObjectId;
import phase6.LocalObjectId;
import phase6.MachineAddress;
import phase6.MyObserver;
import phase6.ObjectDB;
import phase6.PeerCommunicator;
import phase6.PortNumber;

/**
 * The central GUI used to display information about the HyPeerWeb and debug
 * information
 * 
 * @author Matthew Smith
 * 
 */
public class GUI extends JFrame implements MyObserver {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6422946815634467693L;
	private static GUI singleton = null;
	private static GUIProxy proxy;
	private static PortNumber port;
	private static GlobalObjectId globalObjectId;

	/** Main Debugger Panel **/
	private HyPeerWebDebugger debugger;

	private gui.Main.HyPeerWeb hypeerweb;
	private JScrollPane scrollPane;

	/**
	 * Creates and initializes the GUI as being the root
	 */

	public GUI(HyPeerWeb hypeerweb) {
		this.hypeerweb = hypeerweb;
		this.setTitle("HyPeerWeb DEBUGGER V 1.1");

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				shutdown();
				System.exit(0);
			}
		});

		proxy = new GUIProxy(globalObjectId);
		debugger = new HyPeerWebDebugger(this);
		scrollPane = new JScrollPane(debugger);
		scrollPane.setPreferredSize(new Dimension(debugger.WIDTH + 20,
				debugger.HEIGHT));

		this.getContentPane().add(scrollPane);

		this.pack();
	}

	public static GUIProxy getProxy() {
		return proxy;
	}

	private void shutdown() {
		// TODO : Add close method
		// System.out.println("BROKEN!! PLEASE FIX ME OR DIE TRYING. IMPLEMENT THE HYPEERWEBCLOSE METHOD PLOX");
		// hypeerweb.close();
	}

	public static GUI getSingleton(HyPeerWeb hypeerweb) {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			// If Nimbus is not available, you can set the GUI to another look
			// and feel.
		}
		if (singleton == null) {
			try {
				singleton = new GUI(hypeerweb);

				ObjectDB.setFileLocation("gui" + port + ".db");
				ObjectDB.getSingleton().store(
						globalObjectId.getLocalObjectId(), singleton);
				singleton.setVisible(true);
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR",
						JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
				// System.out.println("BROKEN!! PLEASE FIX ME OR DIE TRYING. IMPLEMENT THE HYPEERWEBCLOSE METHOD PLOX");
				// hypeerweb.close();
				System.exit(1);
			}
		}
		return singleton;
	}

	/**
	 * Start Point of the Program
	 */
	public static void main(String[] args) {
		// need to set the machineName and port from passed in params?
		// String machineName = "";

		ObjectDB.setFileLocation("gui" + port + ".db");
		try {
			port = new PortNumber(Integer.parseInt(args[0]));
		} catch (Exception e) {
			Random rand = new Random();
			int min = 50000, max = 60000;

			int randomNum = rand.nextInt(max - min + 1) + min;
			port = new PortNumber(randomNum);
		}
		// try {
		// connect = new PortNumber(Integer.parseInt(args[1]));
		// } catch (Exception e) {
		// connect = new PortNumber(50505);
		// }

		PortNumber.setApplicationsPortNumber(port);
		// String who;
		// try {
		// who = InetAddress.getLocalHost().getHostAddress();
		// MachineAddress.setMachineAddress(InetAddress.getLocalHost()
		// .getHostAddress());
		// } catch (UnknownHostException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// who = "127.0.1.1";
		// }
		// connectionId = new GlobalObjectId(who, connect,
		// new LocalObjectId());
		MachineAddress.setMachineAddress(GlobalObjectId.getMyIp());
		globalObjectId = new GlobalObjectId(GlobalObjectId.getMyIp(),
				new PortNumber(port), new LocalObjectId());
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				HyPeerWeb h = new HyPeerWeb();
				GUI.getSingleton(h);
				// ObjectDB.getSingleton().store(globalObjectId.getLocalObjectId(),
				// me);
				// h.addObserver(new GUIProxy());
				PeerCommunicator.createPeerCommunicator(port);
				System.out.println("First Dump: ");
				ObjectDB.getSingleton().dump();
			}
		});
	}

	/**
	 * Retrieves the HyPeerWeb Debugging Panel
	 * 
	 * @return HyPeerWebDebugger
	 */
	public HyPeerWebDebugger getHyPeerWebDebugger() {
		return debugger;
	}

	public gui.Main.HyPeerWeb getHyPeerWeb() {
		return hypeerweb;
	}

	public void setHyPeerWeb(gui.Main.HyPeerWeb h) {
		hypeerweb = h;
	}

	public void printToTracePanel(Object msg) {
		debugger.getTracePanel().print(msg);
	}

	@Override
	public void finalize() {
		// System.out.println("BROKEN!! PLEASE FIX ME OR DIE TRYING. IMPLEMENT THE HYPEERWEBCLOSE METHOD PLOX");
		// hypeerweb.close();
	}

	@Override
	public void update(String arg0) {
		NodeListing listing = getHyPeerWebDebugger().getMapper()
				.getNodeListing();
		System.out.println("Update called!");
		String method = arg0;
		if (method.equals("add")) {
			listing.increaseListSize();
		} else if (method.equals("remove")) {
			listing.decreaseListSize();
		}
		// if (m.getValue() <= listing.listSize()) { // removed a node
		// listing.decreaseListSize();
		// }
		// if (m.getValue() > listing.listSize()) { // added a node
		// listing.increaseListSize();
		// }
	}

}
